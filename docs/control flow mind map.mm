<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1377457236515" ID="ID_1870632132" MODIFIED="1377475755171" STYLE="bubble" TEXT="Control Flow Lesson">
<node CREATED="1377457370546" ID="ID_815879068" MODIFIED="1377468719343" POSITION="right" STYLE="bubble" TEXT="Examples">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377460640406" ID="ID_1038094671" MODIFIED="1377474776390" STYLE="bubble" TEXT="if else elseif">
<arrowlink DESTINATION="ID_1038094671" ENDARROW="Default" ENDINCLINATION="0;0;" ID="Arrow_ID_1165408711" STARTARROW="None" STARTINCLINATION="0;0;"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377460646312" ID="ID_672985538" MODIFIED="1377468685171" STYLE="bubble" TEXT="control direction of an animation based on even or odd, or some other criteria"/>
<node CREATED="1377460665765" ID="ID_1932259795" MODIFIED="1377468685171" STYLE="bubble" TEXT="an example that is a series of wires. Navigate through a maze with if/else statements"/>
</node>
<node CREATED="1377467797312" ID="ID_565894935" MODIFIED="1377468715828" STYLE="bubble" TEXT="for repeat">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377467806781" ID="ID_1432603159" MODIFIED="1377468685171" STYLE="bubble" TEXT="draw N squares"/>
<node CREATED="1377468014250" ID="ID_1580168315" MODIFIED="1377468685171" STYLE="bubble" TEXT="draw N squares of increasing size"/>
<node CREATED="1377470162546" ID="ID_718859458" MODIFIED="1377470527671" TEXT="concept">
<node CREATED="1377470167218" ID="ID_340476740" MODIFIED="1377470212312" TEXT="for loop is a loop (e.g. steps #1-3, step #4 is return to step #1 until finished)"/>
<node CREATED="1377471724078" ID="ID_96051131" MODIFIED="1377471731656" TEXT="algorithms; what needs to be repeated?">
<node CREATED="1377471733953" ID="ID_466841957" MODIFIED="1377471740328" TEXT="counting multiple numbers"/>
</node>
</node>
</node>
<node CREATED="1377468023703" ID="ID_69921988" MODIFIED="1377468715828" STYLE="bubble" TEXT="nested for loop">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377468027984" ID="ID_651448979" MODIFIED="1377468685171" STYLE="bubble" TEXT="draw N x M squares of increasing size">
<node CREATED="1377468073078" ID="ID_1467877513" MODIFIED="1377468685171" STYLE="bubble" TEXT="vary increasing size direction"/>
</node>
<node CREATED="1377468036359" ID="ID_963865454" MODIFIED="1377468685171" STYLE="bubble" TEXT="draw a grid of x * y asterisks"/>
</node>
<node CREATED="1377468538218" ID="ID_1435090693" MODIFIED="1377468715828" STYLE="bubble" TEXT="while loop">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377468544609" ID="ID_991700974" MODIFIED="1377468685171" STYLE="bubble" TEXT="breakdown parts of for loop (initialize, condition, increment)"/>
<node CREATED="1377468602968" ID="ID_1383701929" MODIFIED="1377468685171" STYLE="bubble" TEXT="alternative while condition">
<node CREATED="1377468627156" ID="ID_402760137" MODIFIED="1377468685171" STYLE="bubble" TEXT="ball bounces or stops when it hits wall"/>
<node CREATED="1377468745671" ID="ID_1265592546" MODIFIED="1377468795937" TEXT="ball increases in size, stops when too big for screen"/>
</node>
</node>
</node>
<node CREATED="1377527481453" ID="ID_1266142424" MODIFIED="1377527485859" POSITION="right" TEXT="Concepts">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377527487015" ID="ID_1381335088" MODIFIED="1377527490750" TEXT="if else elseif">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377527491562" ID="ID_288666644" MODIFIED="1377527551687" TEXT="sometimes you need to make a decision about what to do. Go left, right, or up?"/>
<node CREATED="1377527563312" ID="ID_1356296191" MODIFIED="1377527960531" TEXT="use a boolean expxression to make the decision"/>
<node CREATED="1377527999187" ID="ID_1768618300" MODIFIED="1377528017718" TEXT="example in for loop: print different character based on loop position"/>
</node>
<node CREATED="1377527776687" ID="ID_1716823812" MODIFIED="1377528328734" TEXT="for">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377527781156" ID="ID_1393049539" MODIFIED="1377528037515" TEXT="when you need to do something more than once">
<node CREATED="1377528041656" ID="ID_455243394" MODIFIED="1377528066437" TEXT="factorial"/>
<node CREATED="1377528068468" ID="ID_1743468763" MODIFIED="1377528084687" TEXT="check if a number is prime"/>
<node CREATED="1377528254796" ID="ID_658137611" MODIFIED="1377528264562" TEXT="printing a pattern or graph"/>
<node CREATED="1377528271718" ID="ID_316524046" MODIFIED="1377528292609" TEXT="visual illustrations of these examples, repetitive nature">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1377528322750" ID="ID_42835561" MODIFIED="1377528334968" TEXT="patterns of numbers">
<node CREATED="1377528336031" ID="ID_86807137" MODIFIED="1377528349703" TEXT="different ranges of numbers through multiplication">
<node CREATED="1377528561937" ID="ID_182510021" MODIFIED="1377528568093" TEXT="spiral example"/>
<node CREATED="1377528568359" ID="ID_1959484053" MODIFIED="1377528596687" TEXT="table of 0- and result range (domain and range)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1377529777796" ID="ID_1769058420" MODIFIED="1377529780609" TEXT="alternating with modulus"/>
</node>
</node>
<node CREATED="1377529793468" ID="ID_1451735326" MODIFIED="1377529797937" TEXT="nested for loop">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377529801843" ID="ID_633398128" MODIFIED="1377530015953" TEXT="repeating a loop with variations">
<node CREATED="1377530006328" ID="ID_279214645" MODIFIED="1377530305406" TEXT="visual or animation of creating a color table (5 base colors, feed 5 alternate colors into base)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1377530313265" ID="ID_1291504248" MODIFIED="1377530364609" TEXT="triangle of stars (using outer loop&apos;s variable in inner loop&apos;s condition)">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1377529813859" ID="ID_644126052" MODIFIED="1377530476875" TEXT="a grid of numbers such as a multiplication table"/>
<node CREATED="1377530394250" ID="ID_360862626" MODIFIED="1377530466703" TEXT="introduce complexity"/>
</node>
<node CREATED="1377530534750" ID="ID_104571831" MODIFIED="1377531258406" TEXT="while loop">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node CREATED="1377530537953" ID="ID_1292001632" MODIFIED="1377531254390" TEXT="loop that continues while a condition is true">
<node CREATED="1377530785375" ID="ID_845644299" MODIFIED="1377530811968" TEXT="visual of check - code - repeat loop">
<font ITALIC="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1377530815250" ID="ID_1322833394" MODIFIED="1377531012062" TEXT="write a for loop with a while loop"/>
</node>
</node>
</node>
</map>
