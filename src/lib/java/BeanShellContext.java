import java.io.IOException;

import bsh.EvalError;
import bsh.Interpreter;

public class BeanShellContext {

	public static void main(String[] args) {
		if (args.length == 0) { System.out.println("error. no arguments."); return; }

		Interpreter bsh = new Interpreter();
		

		try {
			bsh.setOut(System.out);
			bsh.setStrictJava(true);

			for (String arg : args) {
				try {
					bsh.source(arg);
				} finally {
					System.out.print("----++++====****----");
				}
			}

		} catch (EvalError e) {
			System.err.println("error. " + e.getMessage() + " at Line: " + e.getErrorLineNumber());
		} catch (IOException e) {
			System.err.println("error. io. " + e.getMessage());
		}
	}
}
