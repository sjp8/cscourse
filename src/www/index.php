<!doctype html>

<html lang='en'>

<head>
	<title>CS Course</title>

	<!-- libraries -->
	<script src='js/lib/jquery.min.js' type='text/javascript'></script>
	<script src='js/lib/jqueryui.js' type='text/javascript'></script>
	<script src='js/lib/jquery.inheritance.js' type='text/javascript'></script>
	<script src='js/lib/jquery.resize.js' type='text/javascript'></script>
	<script src='js/lib/underscore.js' type='text/javascript'></script>
	<script src="js/lib/ace/ace.js" charset="utf-8" type='text/javascript'></script>
	<script src='js/lib/processing.js' charset='utf-8' type='text/javascript'></script>
	<script src='js/lib/js-yaml.js' charset='utf-8' type='text/javascript'></script>
	<script src='js/jquery_addons.js'></script>
	<script src='js/ace_addons.js'></script>

	<link rel="stylesheet" type="text/css" href="css/jqueryui.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />

	<link rel='stylesheet/less' type='text/css' href='css/progress.less' />
	<link rel='stylesheet/less' type='text/css' href='css/lessons.less' />
	<link rel='stylesheet/less' type='text/css' href='css/homework.less' />
	<script src='js/lib/less.js'></script>
</head>

<body>

	<div class='container'>

		<div class='progress-container'>
			<h1 class='progress-title'>Computer Science in Java</h1>
			<div class='progress-sections-container'>
				<h3>Lessons</h3>
				
				<ul class='legend'>
					<div>Legend:</div>
					<li class='type-general'>General Instruction</li>
					<li class='type-java'>Java Instruction</li>
					<li class='type-practice'>Practice Problems</li>
				</ul>

				<ul class='sections'>
					<li course='ComputerFundamentalsLesson'>Computer Fundamentals</li>
					<li course='ProgramStructureLesson'>Program Structure</li>
					<li course='StringsLesson'>Strings</li>
					<li course='MathematicalExpressionsLesson'>Mathematical Expressions</li>
					<li course='VariablesLesson'>Variables</li>
					<li course='BooleanExpressionsLesson'>Boolean Expressions</li>
					<li course='ControlFlowLesson'>Control Flow (if, for, while)</li>
					<li course='AdvancedMethodsLesson'>Advanced Methods</li>
					<li course='FileIOLesson'>File Input and Output (IO)</li>
					<li course='ArraysLesson'>Arrays</li>
					<li course='ListSortAlgorithmsLesson'>Lists, Sorting, and Array Algorithms</li>
					<li course='ClassesLesson'>Classes and Objects</li>
					<li course='ClassInheritanceLesson'>Class Inheritance</li>
					<li course='DraftingToolsLesson'>John-Paul's Drafting Tools</li>
				</ul>
			</div>
			<div class='progress-portfolio-container'>
				<h3>Portfolio</h3>
				<ul class='progress-portfolio-list'>
					<li></li>
				</ul>
			</div>
		</div>

		<div class='lesson-container' style="display: none;">
			<div id="lesson"></div>
		</div>

		<div class='home-button' style='display: none;'>Return Home</div>

		<div id='test_div' style='display: none;'></div>

	</div>

	<script src='js/yaml_lesson_parser.js'></script>
	<script src='js/progress.js'></script>
	<script src='js/variable_figure.js'></script>
	<script src='js/keypress_handler.js'></script>
	<script src='js/addons.js'></script>
	<script src='js/homework.js'></script>
	<script src='js/lesson.js'></script>
	<script src='js/lesson_element.js'></script>
	<script src='js/grammar.js'></script> <!-- depends on lesson_element.js -->
	<script src='js/lesson_page.js'></script>
	<script src='js/lesson_column.js'></script>
	<script src='js/lessons/tools.js'></script>
	<script src='js/lessons/intro.js'></script>
	<script src='js/lessons/programstructure.js'></script>
	<script src='js/lessons/computerfundamentals.js'></script>
	<script src='js/lessons/strings.js'></script>
	<script src='js/lessons/mathexpressions.js'></script>
	<script src='js/lessons/variables.js'></script>
	<script src='js/lessons/booleanexpressions.js'></script>
	<script src='js/lessons/controlflow.js'></script>
	<script src='js/lessons/advancedmethods.js'></script>

	<script>

		var lesson;
		$(function() {
			var hash = window.location.hash;
			var param = function(name) {
				var r = null;
				var s = '/' + name + '/';
				if (hash.indexOf(s) !== -1) {
					var r = hash.substring(hash.indexOf(s) + s.length, hash.length);
					if (r.indexOf('/') !== -1) {
						r = r.substring(0, r.indexOf('/'));
					}
				}
				return r;
			};

			var loadLesson = function(course) {
				$('.progress-container').hide();
				$('.lesson-container, .home-button').show();
				$('#lesson').empty();
				lesson = new window[course];
				var startingPage = Number(param('page')) || 0;
				lesson.start(startingPage);
				window.location.hash = '!/course/' + course + '/page/' + startingPage;
			};

			var loadHome = function() {
				$('.progress-container').show();
				$('.lesson-container, .home-button').hide();
				$('#lesson').empty();
				window.location.hash = '!/home/';
			}

			$('.progress-container *[course]').click(function() { loadLesson($(this).attr('course')); });
			$('.home-button').click(function() { loadHome(); });

			if (hash.length <= 1 || window.location.hash.indexOf('!/home/') === 1) {
				loadHome();
			} else if (window.location.hash.indexOf('!/course/') === 1) {
				loadLesson(param('course'));
			} else if (window.location.hash.indexOf('!/experiment/') === 1) {

				var experiment = param('experiment');

				if (experiment == 'homework') {
					var homework = new Homework(null, $('#test_div').show());
					homework.show();
					$('.progress-container').hide();
				} else if (experiment == 'grammar') {
					var grammar = new GrammarGame({
						":Class": {
							"type": "block",
							"required": true,
							"help": "A class contains a collection of instructions for the computer, organized into sections called methods. The entry point for the program is called the Main method.",
							"definition": [
								["class ", ":ClassName", " {", ":MainMethod", ":Method", "}"]
							]
						},
						":ClassName": {
							"type": "input",
							"required": true,
							"pattern": /^[A-Z][A-z]*$/,
							"help": "A class name starts with a capital letter, with the letter of each word capitalized.. e.g. MyClass. Spaces are not allowed. The class name describes the purpose of the class (e.g. PrintingProgram, FirstProgram)."
						},
						":MainMethod": {
							"type": "block",
							"required": true,
							"help": "The main method is a special type of method. It is where the program starts.",
							"definition": [
								["public ", "static ", "void ", "main ", "(String[] args) ", "{", ":Instructions", "}"]
							]
						},
						":Method": {
							"type": "block",
							"multiple": true,
							"required": false,
							"help": "A method is a named set of code that can be called from other methods.",
							"definition": [
								["static ", "void ", ":MethodName", "(", ") ", "{ ", ":Instructions", "}"]
							]
						},
						":MethodName": {
							"type": "input",
							"required": true,
							"pattern": /^[a-z][A-z]*$/,
							"help": "A method name starts with a lowercase letter, with the letter of each following word capitalized.. e.g. myMethodName. Spaces are not allowed. The method name describes its purpose. It is usually a verb, whereas the class is a noun."
						},
						":Instructions": {
							"type": "block",
							"help": [
								"This instruction prints words made up of numbers and letters (called a String) to the console.",
								"This instruction 'calls' a method that you have written (write a method by dragging a :Method to the left). Calling a method means telling it to run its instructions."
							],
							"multiple": true,
							"definition": [
								["System.out.println", "(", ":String", ")", ";"],
								[":MethodName", "()", ";"]
							]
						},
						":String": {
							"required": true,
							"help": "A string is a series of letters and numbers. In a program, strings are enclosed (at the start and end) with \".",
							"type": "inline",
							"definition": [
								['"', ":Value", '"']
							]
						},
						":Value": {
							"required": true,
							"help": "Enter any letters and numbers. The letters you type will be enclosed by a \" on either side, to denote a String.",
							"type": "input",
							"pattern": /[A-z0-9&%$#@!]*/
						}
					});

					$('#test_div').append(grammar.element);
				} else if (experiment == 'processing') {
					$('<canvas />').attr('data-processing-sources', 'js/processing/variables_clock.pde').appendTo($('#test_div'));

				} else if (experiment == 'raphael') {
					// old experiment: raphael demo
					var paper = Raphael('test_div', 350, 700, function() {
						var r = this;
						r.path("M20,20 L20,50");
					});
				} else if (experiment == 'recorder') {
					
				} else if (experiment == 'robot') {
					Raphael(function() {
						var paper = Raphael('test_div', 400, 400, function() {
							var r = this;
							var space = 35;
							var numbers = {};
	
							var binary = function(digits) {
								digits = digits || 1;
								return _(_.range(0, digits)).map(function() { return '' + _.compose(Math.round, Math.random)(); }).join('');
							};
							var drawNumber = function(x, y, text) {
								numbers[x + ',' + y] = r.text(x, y, text);
							};

							var removeNumber = function(x, y) {
								var num = numbers[x + ',' + y];
								num && num.remove();
							};

							_(_.range(1, 11)).each(function(i) {
								_(_.range(1, 11)).each(function(j) {
									if (Math.random() > 0.7) {
										drawNumber(i * space, j * space, binary(3));
									}
								});
							});

							var robotSet = r.set();
							var path = r.circle(0, 0, 20);
							path.click(function() {
								robotSet.animate({ transform: "t100,100..." }, 1000);
							});

						});
					});
				}
				
				$('#test_div').show();
			}

			//window.onbeforeunload = function() {
			//	return 'If you leave the page, you will lose your progress in this lesson.';
			//};
		});

	</script>


</body>

</html>
