
var VariableTableFigure = Class.extend({

	init: function(container) {
		this.container = container;

		this.title = $('<div />').addClass('variables-title').appendTo(this.container);
		this.variables = $('<div />').addClass('variables-area').appendTo(this.container);

		this.container.removeClass('variable-figure').addClass('variable-table');
		this.table = $('<table />').appendTo(this.variables);
		this.table.append($('<thead />').append($('<tr />')
			.append($('<th />').text('Name').addClass('variable-header-name'))
			.append($('<th />').text('Value').addClass('variable-header-value'))
			.append($('<th />').text('Type').addClass('variable-header-type'))
		));

		this.tableBody = $('<tbody />').appendTo(this.table);
		this.rows = {};
		this.log = [];
		this.vars = {};
	},

	setTitle: function(title) {
		this._title = title;
		this.title.html(title);
	},
	
	getVariables: function() {
		return _.map(this.vars, function(row) { return { name: row.name, type: row.type, value: row.value }; });
	},

	setVariable: function(name, value, type) {
		if (!this.rows[name]) {
			$var  = $('<tr />').addClass('variable-row').attr('name', name).appendTo(this.tableBody);
			$name = $('<td />').addClass('variable-name').appendTo($var);
			$val  = $('<td />').addClass('variable-value').appendTo($var);
			$type = $('<td />').addClass('variable-type').appendTo($var);
			this.rows[name] = { row: $var, name: $name, value: $val, type: $type };
			this.vars[name] = { name: name, value: value, type: type };
			this.log.push('Added "' + name + '", set to ' + value + '.');
		} else {
			this.log.push('Set "' + name + '" to ' + value + '.');
		}

		this.vars[name].value = value;

		var row = this.rows[name];
		row.name.html(name);
		row.value.html(value);
		row.type.html(type);
	},

	clear: function() {
		this.rows = {};
		this.vars = {};
		this.tableBody.empty();
	}

});
