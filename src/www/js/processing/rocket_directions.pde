
int i = 0;
int frame = 0;
float currentX;
float currentY;
float lastX;
float lastY;
int currentDirection;
ArrayList instructions;
ArrayList lines;
int numDirections = 8;
int moveDistance = 20;


void turnLeft() { instructions.add("turnLeft"); }
void turnRight() { instructions.add("turnRight"); }
void moveForward() { instructions.add("moveForward"); }
void teleportRight() { instructions.add("teleportRight"); }

void setup() {

    background(230);
    size(350, 150);

    currentX = 350 / 3; lastX = currentX;
    currentY = 150 / 2; lastY = currentY;
    currentDirection = 0;

    instructions = new ArrayList();
    lines = new ArrayList();

    new <%= className %>().main(null);

    var fps = max(15, min(50, instructions.size()));
    frameRate(fps);
}

void draw() {

    fill(230);
    background();

    for (int j = 0; j < lines.size(); j++) {
        var l = (LineData)lines.get(j);
        strokeWeight(1);
        stroke(0);
        line(l.x0, l.y0, l.x1, l.y1);
    }

    if (i >= instructions.size()) {
        noLoop();
        return;
    }

    String instruction = instructions.get(i);

    if (instruction == "teleportRight") {
        currentX += 350 / 3;
        lastX = currentX;
        lastY = currentY;
        i++;
    } else if (instruction == "turnLeft") {
        currentDirection = (currentDirection + numDirections - 1) % numDirections;
        i++;
    } else if (instruction == "turnRight") {
        currentDirection = (currentDirection + 1) % numDirections;
        i++;
    } else if (instruction == "moveForward") {
        if (frame >= moveDistance) {
            lines.add(new LineData(lastX, lastY, currentX, currentY));
            lastX = currentX;
            lastY = currentY;
            i++;
            frame = 0;
        } else {
            currentX += -cos(PI / 2 + currentDirection * PI / (numDirections / 2));
            currentY += -sin(PI / 2 + currentDirection * PI / (numDirections / 2));
            strokeWeight(1);
            stroke(0);
            line(lastX, lastY, currentX, currentY);
            frame++;
        }
    }

    pushMatrix();

    translate(currentX, currentY);
    rotate(PI + currentDirection * PI / (numDirections / 2));
    translate(0, -20);
    stroke(color(255, 30, 70));
    strokeWeight(10);

    int y = currentY;
    int x = currentX;
    beginShape(); vertex(-5, 15); vertex(0, 23); vertex(5, 15); vertex(5, 10); vertex(-5, 10); endShape(CLOSE);
    stroke(color(random(200, 230), random(160, 200), 5));
    translate(0, -5);
    beginShape(); vertex(3, 0); vertex(5, -5); vertex(0, -2); vertex(-5, -5); vertex(-3, 0); endShape(CLOSE);

    popMatrix();
}

class LineData {
    float x0;
    float x1;
    float y0;
    float y1;
    LineData(float xStart, float yStart, float xEnd, float yEnd) {
        x0 = xStart;
        x1 = xEnd;
        y0 = yStart;
        y1 = yEnd;
    }

}
