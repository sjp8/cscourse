

ArrayList sprouts;
ArrayList instructions;
String instruction; // chosen instruction
String[] directions; // directions for the user

int[] storedValues;
int currentStoredValue;
int totalStoredValues;

int stage;
color[] stageColors;
Robot robot;

int[] numbersArea;

String operationDescription;


void setup() {
	size(400, 300);

	frameRate(20);

	//             x0  y0  x1   y1
	numbersArea = {50, 140, 300, 250};

	stage = 0;
	currentStoredValue = 0;
	
	

	/*
		stages:
		0 choose an instruction
		1 grab a value
		2 grab another value
		3 choose a destination (click)
	*/

	// draw some values on the screen
	sprouts = new ArrayList();
	int[] numbers = {0, 1, 11, 5, 3, 2, 7, 1 };
	for (int i = 0; i < numbers.length; i++) {
		int x = numbersArea[0] + i * ((numbersArea[2] - numbersArea[0]) / numbers.length);
		int y = numbersArea[1] + (numbersArea[3] - numbersArea[1]) / 3 * (cos(i / numbers.length / 1.3 * TWO_PI) + 1) / 2;
		sprouts.add(new Sprout(x, y, "" + numbers[i], color(50)));
	}

	instructions = new ArrayList();

	color instructionColor = color(50, 100, 50);
	instructions.add(new Sprout(25, 60, "Add", instructionColor));
	instructions.add(new Sprout(90, 60, "Copy", instructionColor));
	instructions.add(new Sprout(165, 60, "Delete", instructionColor));

	storedValues = new int[2];

	directions = {
		"To start, choose an operation for the processor to perform.",
		"The processor needs input on what to calculate.",
		"Place the result of the operation somewhere in memory."
	};

	robot = new Robot();
}

void draw() {
	background(255);

	stroke(0, 0, 0);
	fill(240, 255, 240);
	rect(numbersArea[0], numbersArea[1], numbersArea[2] - numbersArea[0], numbersArea[3] - numbersArea[1]);


	if (stage == 0) {
		fill(255, 200, 200);
	} else if (stage == 1) {
		fill(200, 255, 200);
	} else if (stage == 2) {
		fill(200, 200, 255);
	} else if (stage == 3) {
		fill(255, 200, 255);
	}

	stroke(100);

	robot.display(mouseX, mouseY);

	for (int i = 0; i < sprouts.size(); i++) {
		sprouts.get(i).display();
	}

	if (stage == 0)
		for (int i = 0; i < instructions.size(); i++) instructions.get(i).display();

	fill(0, 0, 100);
	textSize(14);
	text(directions[stage], 0, 50);

	textSize(20);
	text("You are a program. ", 0, 25);

	if (operationDescription) {
		fill(50, 50, 120);
		textSize(14);
		text(operationDescription, 20, 100);
	}
	
	textSize(12);
	text("Memory", numbersArea[2] - textWidth("Memory"), numbersArea[1] - textAscent("Memory") / 2);

}

void mouseClicked() {

	if (stage == 0) {
		for (int i = 0; i < instructions.size(); i++) {
			if (instructions.get(i).within(mouseX, mouseY)) {
				instruction = instructions.get(i).name;
				robot.setSlot(3, instruction);
				currentStoredValue = 0;
				stage = 1;

				if (instruction == "Add") {
					totalStoredValues = 2;
					operationDescription = "Add takes two numbers, adds them, and \nstores the result in memory.\nClick on two numbers to add.";
				} else if (instruction == "Copy") {
					totalStoredValues = 1;
					operationDescription = "Copy takes a number, and copies it to a \nnew location in memory.\nClick on a number to copy.";
				} else if (instruction == "Delete") {
					totalStoredValues = 0;
					operationDescription = "Delete removes a number from memory.\nClick on a number to delete.";
					stage = 1;
				}
			}
		}

	} else if (stage == 1) {
		for (int i = sprouts.size() - 1; i >= 0; i--) {
			if (sprouts.get(i).within(mouseX, mouseY)) {
				int operand = int(sprouts.get(i).name);
				robot.setSlot(currentStoredValue + 1, "" + operand);
				storedValues[currentStoredValue] = operand;

				currentStoredValue++;

				if (currentStoredValue == totalStoredValues) {
					stage = 2; // next stage
				}

				if (instruction == "Delete") {
					sprouts.remove(i);
					resetStage();
				}

				break;
			}
		}

	} else if (stage == 2) {
	
		if (mouseX >= numbersArea[0] && mouseX <= numbersArea[2] && mouseY >= numbersArea[1] && mouseY <= numbersArea[3]) {
			int compResult;
	
			if (instruction == "Add") {
				compResult = storedValues[0] + storedValues[1];
				addNewSprout("" + compResult);
			} else if (instruction == "Copy") {
				compResult = storedValues[0];
				addNewSprout("" + compResult);
			} else {
				return;
			}
	
			resetStage();
		}
	}
}

void resetStage() {
	robot.resetSlots();
	operationDescription = "";
	stage = 0;
}

void addNewSprout(t) {
	sprouts.add(new Sprout(mouseX - textWidth(t) / 2, mouseY - textAscent(t), t, color(50, 50, 255)));
}

class Clickable {
	float x, y;
	float width, height;

	boolean within(xin, yin) {
		boolean inx = xin >= x && xin <= x + width;
		boolean iny = yin >= y && yin <= y + height;
		return inx && iny;
	}
}

class Square extends Clickable {
	String content;
	color f;

	Square(float xin, float yin, float win, float hin, color fi) {
		x = xin;
		y = yin;
		width = win;
		height = hin;
		f = fi;
	}

	void setText(t) {
		content = t;
	}

	void setFill(c) {
		f = c;
	}

	void display() {
		rectMode(CORNER);
		fill(f);
		rect(x, y, width, height, floor(height / 5));
		if (content) {
			textSize(18);
			fill(0);
			text(content, x, y);
		}

	}
}

class Sprout extends Clickable {
	float name;
	color col;

	Sprout(float xin, float yin, String namein, color colorin) {
		x = xin;
		y = yin;
		name = namein;
		textSize(20);
		height = textAscent() + textDescent();
		width = textWidth(name);

		col = colorin;
	}

	void display() {
		fill(col);
		text(name, x, y + height);
	}
}

class Robot {
	String[] slots;

	Robot() {
		resetSlots();
	}

	void resetSlots() {
		slots = { "", "", "", "" };
	}

	void setSlot(slot, t) {
		slots[slot] = t;
	}

	void display(x, y) {

		rectMode(CENTER);

		pushMatrix();

		translate(x, y);

		fill(255, 200, 180);
		rect(0, 0, 50, 50); // main body

		int distance = 40;
		drawMini(45, distance, slots[0]);
		drawMini(135, distance, slots[1]);
		drawMini(225, distance, slots[2]);
		drawMini(315, distance, slots[3]);

		popMatrix();

		rectMode(CORNER);
	}

	void drawMini(angle, distance, t) {
		pushMatrix();
		rotate(radians(angle));
		translate(distance, 0);
		fill(240, 200, 100);
		rect(0, 0, 10, 10);

		if (t) {
			textSize(15);
			fill(255, 80, 80);
			translate(10, 0);
			
			rotate(radians(90));
			text(t, 0, 0);
		}

		popMatrix();
	}
}

