
int seconds;
int radius;
ClockDisplay clock;

int _second, _minute;
float _hour;

void setup() {
	size(250, 250);

	frameRate(15);

	radius = 80; // 40 pixel clock radius
	ellipseMode(CENTER);

	clock = new ClockDisplay();
}

void draw() {
	seconds = getSecondOfDay();

	background(240);
	pushMatrix();
	translate(125, 150);

	strokeWeight(3);
	stroke(255, 50, 150);
	fill(255);
	ellipse(0, 0, radius * 2, radius * 2);

	clock.calculateSecond();
	clock.calculateMinute();
	clock.calculateHour();

	stroke(0);
	fill(0);

	popMatrix();

	textAlign(LEFT);
	String calculatedTime = "Calculated Time:\n" +
		"Hour: " + (_hour === null ? 'not set' : floor(_hour * 1000) / 1000.0) +
		"\nMinute: " + (_minute === null ? 'not set' : _minute) +
		"\nSecond: " + (_second === null ? 'not set' : _second);

	text(calculatedTime, 5, 20);
	textAlign(RIGHT);
	float hourDecimal = floor((hour() * 3600 + minute() * 60 + second()) / 3600 * 1000) / 1000.0;
	String actualTime = "Actual Time:\nHour: " + hourDecimal + "\nMinute: " + minute() + "\nSecond: " + second();
	float actualTimeWidth = textWidth(actualTime);
	text(actualTime, width - 5, 20);
}

int getSecondOfDay() {
	return (60 * 60 * hour()) + (60 * minute()) + second();
}

var drawHand(float ratio, float handLength, color c) {
	// ratio is 0 to 1. angle is 90 to 0 to 270 to 180

	float angle = - HALF_PI + ratio * TWO_PI;
	float len = handLength * radius;
	stroke(c);
	strokeWeight(2);
	fill(0);
	line(0, 0, cos(angle) * len, sin(angle) * len);
}

void setSecondHand(int seconds) {
	_second = seconds || 0;
	drawHand(seconds / 60, 0.9, color(255, 100, 100));
}

void setMinuteHand(int minutes) {
	_minute = floor(minutes) || 0;
	drawHand(minutes / 60, 0.7, color(40, 40, 100));
}

void setHourHand(float hour) {
	_hour = hour || 0.0;
	drawHand((hour % 12.0) / 12.0, 0.5, color(0, 0, 0));
}
