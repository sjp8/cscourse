
var LessonElement = Class.extend({
	allElements: {},

	init: function(name, type) {
		this.element = $('<div />').addClass('lesson-element');
		this._column = 0;

		this.type = type;
		name && this.name(name);
		return this;
	},

	byName: function(name) {
		return this.allElements[name];
	},

	// give the element a name
	name: function(name) {
		if (name === undefined) {
			return this._name;
		}
		this._name = name;
		this.element.attr('element-name', name);

		this.allElements[name] = this;
		return this;
	},

	// remove the element on click
	removeOnClick: function() { var context = this; this.element.click(function() { context.fade(false); }); return this; },

	// halting elements halt the display of the page until an action is taken (e.g. continue or wait for finish)
	halting: function() { return false; },

	fade: function(value) {
		if (value === true) {
			this.element.show();
		} else if (value === false) {
			this.element.fadeOut({ queue: false });
		} else if (_.isNumber(value)) {
			this.element.fadeTo(500, value);
		}
	},

	column: function(col) {
		if (col === undefined) { return this._column; }
		else { this._column = col; return this; }
	},

	// insert into presentation
	insert: function(container) {
		this.element.appendTo(container);
		return this;
	},

	// removes information added during insert
	reset: function() {
		if (this.element) {
			this.element.empty().remove();
		}
	},

	onFinish: function(finish) { if (finish === undefined) { return this._onFinish; } else { this._onFinish = finish; return this; } }

});

var FinishElement = LessonElement.extend({
	init: function() { this._super(); this.element.addClass('finish-element'); },
	halting: function() { return true; }
});
var ButtonElement = LessonElement.extend({
	init: function(name) { this._super(name, 'button'); this.element.addClass('button-element'); return this; },
	text: function(text) { this._text = text; return this; },
	click: function(func) { this._click = func; return this; },
	once: function(func) { this._once = func; return this; },
	reset: function() { this.element.removeClass('button-clicked-disabled'); this.element.off(); this._super(); },
	insert: function(container) {
		this.element.html(this._text);
		if (this._click) { this.element.click(_.bind(this._click, this)); }
		if (this._once) {
			this.element.click(_.once(_.bind(function() {
				this._once();
				this.element.addClass('button-clicked-disabled');
			}, this)));
		}

		this._super(container);
	}
});

var ContinueButtonElement = ButtonElement.extend({
	init: function() { this._super(); this.text('Continue'); this.element.addClass('continue-button-element'); },
	halting: function() { return true; }
});

var TextElement = LessonElement.extend({
	init: function() { this._super(); this.element.addClass('text-element'); return this; },
	text: function(text) { this._text = text; return this; },
	hidden: function(text, buttonTitle) { this._hiddenText = text; this._hiddenTextButton = buttonTitle; return this; },
	color: function(color) { this._color = color; return this; },
	size: function(type) { this._size = type; return this; },
	insert: function(container) {
		var self = this;
		if (_.isString(this._text)) {
			this._text = [this._text];
		}

		this.element.html('');
		_.each(this._text, function(text) { self.element.append($('<p />').html(text)); });
		this._color && this.element.css('color', this._color);
		if (this._size) {
			var sizes = { small: '0.8em', normal: '', 'default': '', medium: '', large: '1.2em', larger: '1.4em' };
			this.element.css('font-size', sizes[type] || '');
		}
		
		if (this._hiddenText) {
			var $hiddenButton = $('<div />').addClass('hidden-text-show-button').html(this._hiddenTextButton || 'Hidden').appendTo(this.element);
			$hiddenButton.click(function() {
				$hiddenButton.hide();
				$hidden.fadeIn();
			});
			var $hidden = $('<span />').addClass('hidden-text').hide().html('&nbsp;' + this._hiddenText).appendTo(this.element);
		}

		this._super(container);
	}
});

var ListElement = LessonElement.extend({
	init: function() { this._super(); this.element.addClass('list-element'); this._type = 'ul'; this._items = []; return this; },
	ordered: function() { this._type = 'ol'; return this; },
	items: function(items) {
		this._items = _.map(items || arguments, _.identity);
		return this;
	},

	insert: function(container) {
		var $list = this._type == 'ol' ? $('<ol />') : $('<ul />');
		$list.appendTo(this.element);

		_.each(this._items, function(item) {
			if (_.isObject(item)) { item = $(item).html(); }
			$('<li />').html(item).appendTo($list);
		});

		this._super(container);
	}
});

var TableElement = LessonElement.extend({
	init: function() { this._super(); this.element.addClass('table-element'); this._headers = []; this._rows = []; return this; },
	headers: function() {
		this._headers = _.map(arguments, _.identity);
		return this;
	},
	row: function() {
		var row = _.map(arguments, _.identity);
		this._rows.push(row);
		return this;
	},
	rows: function() {
		var rows = _.map(arguments, _.identity);
		_(rows).each(function(r) { this.row.apply(this, r); }, this);
		return this;
	},
	
	insert: function(container) {
		
		var $table = $('<table/>');
		var $headers = $('<tr />').appendTo($('<thead />').appendTo($table));
		var $rows = $('<tbody />').appendTo($table);

		_.each(this._headers, function(h) {
			if (_.isObject(h)) {
				h = $(h).html();
			}

			$('<th />').html(h).appendTo($headers);
		});
		
		_.each(this._rows, function(row) {
			var $row = $('<tr />').appendTo($rows);
			_.each(row, function(cell) {
				if (_.isObject(cell)) {
					cell = $(cell).html();
				}
				$('<td />').html(cell).appendTo($row);
			});
		});

		this.element.append($table);

		this._super(container);
	}
	
});

var TitleElement = TextElement.extend({
	init: function() { this._super(); this.element.addClass('title-element'); }
});

// abstract class for any diagram classes, which gives the normal lesson methods and the diagram methods
var DiagramElement = LessonElement.extend({
	init: function(diagramType) {
		this._super();

		this.diagramElement = $('<div />');
		this.element.addClass('diagram-element ' + diagramType);

		this.diagram = new window[diagramType](this.diagramElement);

		_(_.methods(this.diagram)).each(function(method, index) {
			if (method != 'init') {
				this[method] = _.bind(function() {
					var a = arguments;
					var result = this.diagram[method].apply(this.diagram, a);
					return result !== undefined ? result : this;
				}, this);
			}
		}, this);
	},

	insert: function(container) {
		this.element.append(this.diagramElement);
		this._super(container);
	}
});

var VariableFigureElement = DiagramElement.extend({ init: function() { this._super('VariableFigure'); } });

var VariableTableElement = DiagramElement.extend({ init: function() { this._super('VariableTableFigure'); } });

var QuizElement = LessonElement.extend({

	init: function() {
		this._super();

		this._question = null;
		this._answers = [];
		this.element.addClass('quiz-element');

		return this;
	},

	multiple: function() { this._multiple = true; return this; },

	answers: function(answers) {
		if (arguments.length > 1) {
			answers = _(arguments).map(_.identity);
		}
		_(answers).each(function(answer) {
			if (_.isArray(answer)) {
				this.addAnswer(answer[0], answer[1], answer[2]);
			} else {
				this.addAnswer(answer.text || answer.question, answer.correct, answer.explanation);
			}
		}, this);
		return this;
	},

	question: function(question) { this._question = question; return this; },
	
	explanation: function(explanation) { this._explanation = explanation; return this; },

	addAnswer: function(text, correct, explanation) {
		this._answers.push({
			text: text,
			correct: correct,
			explanation: explanation
		});
		return this;
	},

	randomize: function() { this._randomize = true; return this; },

	insert: function(container) {
		var self = this;
		this.quizContainer = $('<div />').addClass('quiz-container').appendTo(this.element);
		var question = this._question, answers = this._answers;
		var $quiz = $('<div />').addClass('lesson-quiz quiz-new').appendTo(this.quizContainer);
		var $question = $('<div />').addClass('lesson-quiz-question').html('<span class="quiz-header">Quiz:</span> ' + question).appendTo($quiz);
		var $answers = $('<div />').addClass('lesson-quiz-answers').appendTo($quiz);
		var $answerButton = $('<div />').addClass('check-answer-button lesson-element button-element').text('Check Answer').click(
			_.once(function() {
				$quiz.addClass('quiz-answered').removeClass('quiz-new');
				$quiz.find('.lesson-quiz-explanation').show();
				$answerButton.fadeOut();
				$answers.children().off();
				self.onFinish() && self.onFinish()();
			})
		).appendTo($quiz);
		var $explanation = $('<div />').addClass('lesson-quiz-explanation overall-explanation').html(this._explanation || '').hide().appendTo($quiz);

		if (this._randomize) { answers = _.shuffle(answers); }

		_.each(answers, function(answer, index) {
			var $answer = $('<div />').addClass('lesson-quiz-answer').attr('is-correct', '' + answer.correct).appendTo($answers);
			answer.explanation && $('<div />').addClass('lesson-quiz-explanation').html(answer.explanation).hide().appendTo($answers);
			$answer.attr('answer-index', index);
			$answer.html(answer.text);
			$answer.click(function() {
				if (!self._multiple) {
					$answers.children().removeClass('lesson-quiz-student-answer');
					$answer.addClass('lesson-quiz-student-answer');
				} else {
					$answer.toggleClass('lesson-quiz-student-answer');
				}
			});
		}, this);

		this._super(container);
	}
});

var FunctionElement = LessonElement.extend({
	init: function(func, context) {
		this.func = func;
		this.context = context; // optional
	},
	insert: function() { (this.context ? _.bind(this.func, this.context) : this.func)(); },
	reset: function() { console.error('Function Elements cannot be reset.'); }
});


var DrawingElement = LessonElement.extend({
	draw: function(func) {
		this._draw = func;
		return this;
	},

	insert: function(container) {
		var $drawing = $('<div />').appendTo(this.element);
		var drawing = this._draw($drawing[0]);

		this._super(container);
	}
});

var FileElement = LessonElement.extend({
	init: function() { this._super(); this._dir = 'images/lessons/'; this.element.addClass('file-element'); return this; },
	file: function(src) { this._src = src; return this; },
	dir: function(dir) { this._dir = dir; return this; }
});

var ImageElement = FileElement.extend({
	init: function() { this._super(); this.element.addClass('image-element'); return this; },

	dim: function(width, height, scale) { scale = scale || 1.0; this._dim = { width: width * scale, height: height * scale }; return this; },
	width: function(width) { this._dim = this._dim || {}; this._dim.width = width; return this; },
	height: function(height) { this._dim = this._dim || {}; this._dim.height = height; return this; },

	border: function(style) { this._border = style || '1px solid black'; return this; },

	insert: function(container) {
		var e = this.element;
		var $img = $('<img />').attr('src', this._dir + this._src).appendTo(this.element).css('visibility', 'hidden');

		if (this._dim) {
			var containerWidth = container.width();

			var ratio = this._dim.width / this._dim.height;
			var imageWidth = Math.min(containerWidth, this._dim.width);
			var imageHeight = imageWidth / ratio;
			imageHeight = Math.min(400, imageHeight);

			$img.css('height', imageHeight).css('width', imageWidth);
		}

		if (this._border) {
			$img.css('border', this._border);
		}

		$img.load(function() {
			$img.css('visibility', '');
			$img.fadeIn();
			$img.css('height', '');
		});

		this._super(container);
	}
});

var SVGElement = ImageElement.extend({});

var ProcessingElement = FileElement.extend({
	init: function() { this._super(); this._dir = 'js/processing/'; this.element.addClass('processing-element'); return this; },

	dim: function(width, height, scale) { scale = scale || 1.0; this._dim = { width: width * scale, height: height * scale }; return this; },

	insert: function(container) {
		var $canvas = $('<canvas />'); // 'data-processing-sources'     this._dir + this._src
		var $container = $('<div />').append($canvas);

		if (this._dim) {
			$container.css({ width: this._dim.width, height: this._dim.height });
		}

		this.element.append($container);
		this._super(container);

		var self = this;
		_.delay(function() {
			Processing.loadSketchFromSources($canvas[0], [self._dir + self._src]);
		}, 1000);
	}

});

var EditorElement = LessonElement.extend({
	init: function() { this._super(); this.element.addClass('edit-processing-element'); this._hints = []; },

	// show admin controls for recording
	admin: function() { this._showAdmin = true; return this; },

	// get editor object
	getEditor: function() { return this._editor; },
	getCode: function() { return this.getEditor().getValue(); },

	// set java settings or code, displayed in editor
	javaSettings: function(settings) { this._javaSettings = settings; return this; },
	javaCode: function(code) {
		this._javaCode = code;
		if (this._editor) {
			this._editor.setValue(code, -1);
		}
		return this;
	},
	javaCodeFile: function(file) {
		var self = this;
		$.get(file, function(result) { self.javaCode(result); });
		return this;
	},

	// add hint (Hint button)
	addHint: function(hint) { this._hints.push(hint); return this; },


	// overall run button
	run: function() {
		this.consoleContainer.show().empty();
	},

	// toggle helpers for hidden, and active states (CSS-driven)
	_toggleEnabled: function(element, value) {
		element.toggleClass('state-hidden', !value);
	},
	_toggleActive: function(element, value) {
		element.toggleClass('state-active', !value);
		element.toggleClass('state-inactive', value);
	},

	toggleRunButton: function(value) { this._toggleEnabled(this.runButton, value); },
	togglePlaybackButtons: function(button, slider) {
		this._toggleEnabled(this.element.find('.editor-playback-speed, .editor-playback-speed-value'), slider);
	},

	toggleSkipButton: function(value) { this._toggleEnabled(this.element.find('.edit-button-skip'), value); },
	toggleRecordButton: function(value) { this._toggleActive(this.element.find('.edit-button-record'), value); },


	insert: function(container) {
		var self = this;

		this.editorContainer = $('<div />').addClass('editor-container').appendTo(this.element);
		this.buttonsContainer = $('<div />').addClass('editor-buttons').appendTo(this.element);
		this.adminButtonsContainer = $('<div />').addClass('editor-buttons editor-admin-buttons').appendTo(this.element);
		this.consoleContainer = $('<div />').addClass('console-container').appendTo(this.element).hide();
		this.canvasContainer = $('<div />').addClass('canvas-container').appendTo(this.element);

		//
		// Setup the code editor
		var $editor = $('<div />').css({
			height: 300
		}).appendTo(this.editorContainer);

		var editor = this._editor = ace.edit($editor[0]);
		_.extend(editor, AceAddons);
		editor.setTheme('ace/theme/textmate');
		editor.getSession().setMode('ace/mode/java');
		window.editor = editor; // use in global setting

		var editorInitialValue = '';

		if (this._javaCode) {
			editorInitialValue = this._javaCode;
		} else if (this._javaSettings) {
			var template = _.template('<% if (classComment) { %>/* <%= classComment %> */\n<% } %>class <%= className %> {\n' +
									  '<% if (includeMain) { %>\tpublic static void main(String[] args) {\n\t\t<%= mainCode %>\n\t}\n<% } %>' +
									  '<% _.each(methods, function(m) { %>\tstatic void <%= m.name %>() {\n\t\t<%= m.code %>\n\t}\n<% }) %>' +
									  '}');
			var js = this._javaSettings;
			editorInitialValue = template({ methods: js.methods || [], mainCode: js.mainCode, classComment: js.classComment, className: js.className, includeMain: js.includeMain });
		}

		editor.setValue(editorInitialValue, -1);

		//
		// Setup Run button
		this.runButton = $('<div />').addClass('editor-button editor-button-run').text('Run Your Code').appendTo(this.buttonsContainer);
		this.runButton.click(function() { self.run(); });
		
		this.setupAdminButtons();



		var $playbackSpeed = this.playbackSpeedSlider = $('<div />').addClass('editor-playback-speed').appendTo(this.buttonsContainer);
		var $playbackSpeedValue = this.playbackSpeedValue = $('<div />').addClass('editor-playback-speed-value').text('1.0x Speed').appendTo(this.buttonsContainer);
		var slide = function(e, ui) {
			editor.setTimedPlaybackSpeed(ui.value);
			$playbackSpeedValue.text(ui.value + 'x Speed');
		};
		$playbackSpeed.slider({
			value: 1.0, min: 0.2, max: 5.0, step: 0.1, slide: slide, change: slide
		});

		// Skip playback button
		var $skip = $('<div />').addClass('editor-button edit-button-skip').text('Skip').appendTo(this.buttonsContainer);
		$skip.click(function() {
			editor.completeTimedPlayback();
		});

		//
		// Hint button
		if (this._hints.length > 0) {
			var currentHint = 0;
			var $hints = $('<div />').addClass('editor-button edit-button-hint').text('Hint').appendTo(this.buttonsContainer);
			$hints.click(function() {
				var hint = self._hints[currentHint];
				editor.showMessage(hint);
				currentHint = (currentHint + 1) % self._hints.length;
			});
		}


		this._super(container);
	},
	
	setupAdminButtons: function() {
		var editor = this.getEditor();
		var self = this;

		//
		// Setup recording buttons
		if (this._showAdmin) {
			var $record = this.recordButton = $('<div />').addClass('editor-button edit-button-record').html('&nbsp;').appendTo(this.adminButtonsContainer).click(function() {
				if (this.record) {
					editor.stopRecording();
					editor.sendRecording();
					self.togglePlaybackButtons(true, false);
				} else {
					self.togglePlaybackButtons(false, false);
					editor.startRecording();
				}
				this.record = !this.record;
			});
	
			var $pauseRecord = this.pauseRecordButton = $('<div />').addClass('editor-button edit-button-record-pause').html('Pause Recording').appendTo(this.adminButtonsContainer);
			$pauseRecord.click(function() {
				if (this.paused) {
					// continue recording
					editor.resumeRecording();
					$pauseRecord.text('Pause Recording');
				} else {
					editor.pauseRecording();
					$pauseRecord.text('Resume Recording');
				}
	
				this.paused = !this.paused;
			});

			//
			// Recording extra buttons
			(function() {
				var $messageAdmin = $('<div />').addClass('message-admin-container').appendTo(self.adminButtonsContainer);
				var $input = $('<textarea />').attr('placeholder', 'Message to show.').appendTo($messageAdmin);
				var $delayInput = $('<input />').attr('type', 'text').attr('placeholder', 'Delay (s or ms) (blank = none)').appendTo($messageAdmin);
				var $add = $('<button />').text('Add').appendTo($messageAdmin).click(function() {
					if ($input.val() == '') { return false; }
					editor.addRecordingMessage($input.val(), Number($delayInput.val()) || undefined);
					editor.showMessage($input.val(), 5000);
					$input.val('');
					$delayInput.val('');
					return false;
				});

				var $openfile = $('<div />').addClass('open-playback-container').appendTo(self.adminButtonsContainer);
				var $fileInput = $('<input />').attr('type', 'text').appendTo($openfile);
				$fileInput.keypress(function(e) {
					if (e.which == 13) { // enter
						$.ajax({ url: 'record/' + $fileInput.val(), dataType: 'json', success: function(response) {
							editor.timedPlayback(response.deltas, response.messages);
						} });

						$fileInput.val('');
					}
				});
			})();
		}

		this.toggleRecordButton(true);
		this.togglePlaybackButtons(false, false);
	}
});

var ProcessingEditorElement = EditorElement.extend({

	// hide canvas (visible by default)
	hideCanvas: function() { this._hideCanvas; return this; },
	canvas: function(canvas) { this._canvas = canvas; return this; },

	// set processing code (hidden, included in program run).
	processingCode: function(code) { this._processingCode = code; return this; },
	processingCodeFile: function(file) {
		var self = this;
		$.get(file, function(result) {
			self.processingCode(result);
		});
		return this;
	},

	getCanvas: function() {
		if (this._canvasCache) { return this._canvasCache; }
		var canvas;
		if (_.isFunction(this._canvas)) { canvas = _.bind(this._canvas, this)(); }
		else { canvas = this._canvas; }
		if (canvas[0]) { canvas = canvas[0]; }

		this._canvasCache = canvas;
		return canvas;
	},

	getPreprocessingCode: function(code) {
		var className = '';
		if (code) {
			var match = code.match(/class\s+([a-zA-Z0-9_]+)/);
			if (match && match[1]) {
				className = match[1];
			}
		}
		var processingCode = _.template(this._processingCode)({ className: className });
		return processingCode;
	},

	loggerClass: Class.extend({
		log: function(message) {
			self.consoleContainer.append($('<div />').addClass('log-line').text(message));
		}
	}),

	run: function() {
		this._super();
		var code = this.getCode();
		var processingCode = this.getPreprocessingCode(code);
		Processing.logger = new this.loggerClass();
		if (this._processing) {
			this._processing.noLoop();
		}
		this._processing = new Processing(this.getCanvas(), processingCode + '\n' + code);
		
	},

	insert: function(container) {
		this._super(container);

		if (!this._canvas) {
			this.canvas($('<canvas />').appendTo(this.canvasContainer));
		}

		if (this._hideCanvas) { $(this.getCanvas()).hide(); }
	}

});

var JavaEditorElement = EditorElement.extend({
	run: function() {
		var self = this;
		this._super();

		var code = this.getCode();
		var match = code.match(/class\s+([a-zA-Z0-9_]+)/);
		// evaluate main method of class if present.
		if (match && match.length >= 2) {
			code += '\n\ntry { ' + match[1] + '.main(null); } catch(Exception ex) {  }';
		}

		new Compiler().run(code, function(result, error) {
			_.each(result.split('\n'), function(line) {
				self.consoleContainer.append($('<div />').addClass('log-line').text(line));
			});
			
			if (error) {
				self.consoleContainer.append($('<div />').addClass('log-line log-line-error').text(error));
			}
		});
	}
});

var EditorPlaybackElement = LessonElement.extend({
	init: function() { return this; },

	file: function(file) { this._file = file; return this; },
	editor: function(editor) { this._editor = editor; return this; },
	clear: function(value) { this._clear = value; return this; },

	insert: function() {
		var editorElement = this.allElements[this._editor];
		var editor = editorElement.getEditor();
		if (this._clear) { editor.setValue(''); }

		var self = this;

		$.ajax({ url: 'js/recordings/' + this._file, dataType: 'json',
			success: function(response) {
				editorElement.toggleSkipButton(true);

				editor.timedPlayback(response.deltas, response.messages, function() {
					editorElement.toggleSkipButton(false);
					self.onFinish() && self.onFinish()();
				});
				editorElement.togglePlaybackButtons(true, true);
			}
		});
	}
});

var CommandElement = LessonElement.extend({
	init: function() { return this; },
	commands: function(commands) { this._commands = commands; return this; },
	item: function(name) { this._element = name; return this; },

	insert: function() {
		var element = this.allElements[this._element];
		_.bind(this._commands, element)();
	}
});

/**
 * An Ace Editor element intended to display non-editable code without scrolling,
 * resized to fit content. If height is specified, it will be used as the maximum
 * height.
 */
var CodeElement = LessonElement.extend({
	init: function() { this._super(); this._text = ''; this.element.addClass('code-element'); },

	height: function(height) { this._height = height; return this; },
	text: function(text) { this._text = text; return this; },

	insert: function(container) {
		var $editor = $('<div />').css({
			height: Math.min(this._height || 5000, this._text.split("\n").length * 14) // estimate height
		}).appendTo(this.element);

		var editor = this._editor = ace.edit($editor[0]);
		window.editor = editor;
		_.extend(editor, AceAddons);
		editor.setTheme('ace/theme/textmate');
		editor.getSession().setMode('ace/mode/java');

		editor.resizeToFit(this._height);
		editor.readOnly();
		editor.setValue(this._text, -1);

		this._super(container);
	}
});

/**
 * Console element, accepts input from a textbox and provides contextual Java program output.
 */
var ConsoleElement = LessonElement.extend({

	init: function() {
		this._super();
		this.element.addClass('console-element');
		this.code = [];
		this.history = [];
		this.compiler = new Compiler();
	},
	
	onEvaluate: function(callback) {
		this._onEvaluate = _.bind(callback, this);
		return this;
	},
	
	// calls callback on the input element keyup event.
	onInputUpdate: function(callback) {
		this._onInputUpdate = _.bind(callback, this);
		return this;
	},

	hideOutput: function() { this._hideOutput = true; return this; },

	setError: function(message) {
		this.error && this.error.html(message).show();
	},

	surround: function(template) {
		this._surround = _.template(template);
		return this;
	},
	
	preCode: function(code) {
		this.code.push(code);
		return this;
	},

	expect: function(isInput, value, print, color, onFinish) {
		color = color || 'green';
		this._expect = { isInput: isInput, value: value, print: print, color: color, onFinish: onFinish };
	},

	print: function(text, color, addClass) {
		var $line = $('<div />').addClass((addClass || ' ') + 'console-print-line').text(text).appendTo(this.outputElement);
		if (color) {
			$line.css('color', color);
		}
	},

	evaluate: function(code, onFinish) {
		var self = this;

		var preParseCode = code;

		if (this._surround) {
			code = this._surround({ value: code });
		}

		var _expectMatch = _.bind(function(v, input) {
			if (this._expect && input == this._expect.isInput) {
				if ((_.isRegExp(this._expect.value) && v.match(this._expect.value)) ||
					(_.isString(this._expect.value) && v.indexOf(this._expect.value) !== -1) ||
					(_.isFunction(this._expect.value) && this._expect.value(v))) {

					this._expect.onFinish && this._expect.onFinish();
					this.print(this._expect.print || 'Correct', this._expect.color);
					this._expect = undefined;
				}
			}
		}, this);


		this.compiler.runState(this.code.join('\n'), code, function(oldResult, newResult, error) {

			var _toHtml = function(c) {
				c = c.replace(/\t/g, "<span class='tab'>&nbsp;&nbsp;&nbsp;&nbsp;</span>");
				c = c.replace(/ /g, "&nbsp;");
				return c;
			}

			$('<div />').addClass('console-output-line console-input-line').text(preParseCode).appendTo(self.outputElement);

			var newLines = newResult.split('\n');

			_.each(newLines, function(line) {
				var $line = $('<div />').addClass('console-output-line').appendTo(self.outputElement);
				$line.html(_toHtml(line));
			});

			if (error) {
				var $line = $('<div />').text(error).addClass('console-output-line console-output-error').appendTo(self.outputElement);
				if (this._hideOutput) {
					self.setError(error);
				}
			}

			_expectMatch(preParseCode, true);
			_expectMatch(newResult, false);

			self.outputElement.scrollTop(self.outputElement[0].scrollHeight);

			if (!error) {
				self.code.push(code);
			}

			self.history.unshift(preParseCode);

			if (onFinish) {
				onFinish(preParseCode, newResult, error);
			} else if (self._onEvaluate) {
				self._onEvaluate(preParseCode, newResult, error);
			}
		});

	},

	insert: function(container) {
		var self = this;

		this.outputElement = $('<div />').addClass('console-output').appendTo(this.element);
		this.inputElement = $('<div />').addClass('console-input').appendTo(this.element);
		this.input = $('<input />').attr('type', 'text');
		this._onInputUpdate && this.input.keyup(function() { self._onInputUpdate($(this).val()); });
		this.inputElement.append(this.input);
		
		this.error = $('<div />').addClass('console-error').appendTo(this.element);

		var historyIndex = 0;
		this.input.keydown(function(e) {
			if (e.which == 13) { // enter key
				self.evaluate($(this).val());
				$(this).val('');
			} else if (e.which == 38) { // up key
				$(this).val(self.history[historyIndex] || $(this).val());
				// set to most recent history, then increment
				historyIndex++;
			} else if (e.which == 40) {
				$(this).val(self.history[historyIndex + 1] || '');
				// set to item after most recent history, then decrement (undo)
				historyIndex--;
			} else {
				historyIndex = 0;
				return;
			}
			historyIndex = Math.max(0, Math.min(historyIndex, self.history.length - 1));
		});
		
		if (this._hideOutput) {
			this.outputElement.hide();
		}

		this._super(container);
	}

});

var ConsoleExpectElement = LessonElement.extend({
	init: function() { },
	item: function(element) { this._element = element; return this; },

	expect: function(isInput, value, print, color) {
		this._expect = { input: isInput, value: value, print: print, color: color };
		return this;
	},

	insert: function() {
		var element = this.allElements[this._element];
		var self = this;
		element.expect(this._expect.input, this._expect.value, this._expect.print, this._expect.color, function() {
			self.onFinish() && self.onFinish()();
		});
		
	}
});

var InformationElement = LessonElement.extend({
	init: function() {
		this._super();
		this.info = {};
		this.element.addClass('information-element');
		this.titleElement = $('<div />').addClass('information-title');
		this.infoElement = $('<div />');
	},

	setTitle: function(title) {
		this.titleElement.html(title);
		return this;
	},
	
	clearInfo: function(label) {
		this.info[label].element.remove();
		delete this.info[label];
	},

	setInfo: function(label, value, type, isComplete) {
		type = type || 'text';
		if (!this.info[label]) {
			var element = $('<div />').addClass('information-row type-' + type).appendTo(this.infoElement);
			var labelElement = $('<div />').addClass('information-label').appendTo(element);
			var valueElement = $('<div />').addClass('information-value').appendTo(element);
			this.info[label] = {
				value: value,
				label: label,
				element: element,
				v: valueElement,
				l: labelElement
			};
		}

		var i = this.info[label];
		i.element.toggleClass('information-complete', !!isComplete);
		i.value = value;
		i.v.html(value);
		i.l.html(label);
		return this;
	},

	insert: function(container) {
		this.element.append(this.titleElement).append(this.infoElement);
		this._super(container);
	}

});
