

var GrammarGameElement = LessonElement.extend({
	init: function() {
		this._super();
		this.element.addClass('grammar-game-element');
	},

	height: function(height) { this._height = height; return this; },
	grammar: function(grammar) { this._grammar = grammar; return this; },

	insert: function(container) {
		var grammar = new GrammarGame(this._grammar, this._height);
		this.element.append(grammar.element);

		this._super(container);
	}

});


var GrammarGame = Class.extend({
	init: function(grammar) {
		this.grammar = grammar;
		this.element = $('<div />').addClass('grammar-game clearfix');
		this.areaContainer = $('<div />').addClass('grammar-area-container').appendTo(this.element);
		this.area = $('<div />').addClass('grammar-area').appendTo(this.areaContainer);
		this.code = $('<div />').addClass('grammar-area-code').appendTo(this.areaContainer);
		this.rules = $('<div />').addClass('grammar-rules').appendTo(this.element);
		this.setup();
	},

	height: function(height) { this._height = height; },

	setup: function() {
		var self = this;
		var grammar = this.grammar;

		var firstSymbol = _.first(_.map(grammar, function(expression, symbol) { return symbol; }));

		this.setupRules();
		this.setupCode();

		var $start = $('<div />').attr('grammar-symbol', firstSymbol).text('Drag a ' + firstSymbol + ' from the right to start.').appendTo(this.area);
		$start.css('padding', '2em').css('font-size', '1.2em');

		this.droppable($start);
	},

	addSymbol: function(symbol, rule, definitionIndex, replace) {
		var self = this;
		var $layer = $('<div />').addClass('grammar-layer');

		if (rule.type == 'block') {
			var $layerTitle = $('<div />').addClass('grammar-layer-title').text(symbol).appendTo($layer);
		} else {
			$layer.removeClass('grammar-layer').addClass('grammar-layer-inline');
		}

		if (rule.pattern) {
			var input = -1;
			while (input != null && (input == -1 || !input.match(rule.pattern))) {
				input = prompt(rule.help);
			}

			if (_.isString(input)) {
				var $text = $('<div />').addClass('grammar-input-text').text(input).appendTo($layer);
			} else {
				return; // return early if prompt was empty.
			}

		} else if (rule.definition) {
			var $rules = $('<div />').addClass('grammar-layer-rules').appendTo($layer);

			var ruleset = rule.definition[definitionIndex];
			_(ruleset).each(function(def) {
				if (def[0] == ':') {
					var $term = $('<div />').attr('grammar-symbol', def).addClass('grammar-symbol').text(def).appendTo($layer);
					if (this.grammar[def].required) {
						$term.addClass('grammar-required');
					}
					$term.addClass('grammar-type-' + this.grammar[def].type);
					this.droppable($term);
				} else {
					$('<div />').addClass('grammar-literal').text(def).appendTo($layer);
				}
			}, this);
		}

		if (rule.multiple) {
			replace.before($layer);
		} else {
			replace.removeAttr('grammar-symbol');
			replace.before($layer.hide());
			replace.replaceWithFade($layer, function() { self.updateCode(); });
		}

		// show and hide grammar rules
		var validRules = $('.grammar-area .grammar-symbol[grammar-symbol]').map(function(i, s) { return $(s).attr('grammar-symbol'); }).toArray();
		$('.grammar-rules .grammar-rule').each(function(i, r) { $(r).toggleSlide(_.indexOf(validRules, $(r).attr('grammar-symbol')) !== -1); });
	},

	droppable: function(element) {
		var self = this;

		element.droppable({
			tolerance: 'pointer',
			accept: function(draggable) { return draggable.attr('grammar-symbol') == element.attr('grammar-symbol'); },
			hoverClass: 'grammar-rule-hover',
			drop: function(d, ui) {
				var $rule = $(ui.draggable);
				var symbol = $rule.attr('grammar-symbol');
				var rule = self.grammar[symbol];
				var definitionIndex = $rule.attr('definition-index');
				definitionIndex = definitionIndex ? Number(definitionIndex) : undefined;

				var $target = $(this);
				if ($rule.attr('grammar-rule') == $target.attr('grammar-rule')) {
					self.addSymbol(symbol, rule, definitionIndex, $target);
				}
			}
		});
	},

	showDragIndicators: function(symbol, enable) {
		this.area.find('*[grammar-symbol="' + symbol + '"]').toggleClass('grammar-symbol-acceptor', enable);
	},

	setupRules: function() {
		var grammar = this.grammar;
		var self = this;

		var addRule = _.bind(function(rules, symbol, definitionIndex) {
			// rule is a certain color on hover-over
			var $rule = $('<div />').attr('grammar-symbol', symbol).addClass('grammar-rule').appendTo(this.rules);
			if (definitionIndex !== undefined) {
				$rule.attr('definition-index', definitionIndex);
			}

			var $symbol = $('<div />').addClass('grammar-symbol-title').text(symbol).appendTo($rule);
			if (rules.help) {
				var help = _.isArray(rules.help) ? rules.help[definitionIndex] : rules.help;
				$('<div />').addClass('grammar-help').text(help).appendTo($rule);
			}

			var $terms = $('<div />').addClass('grammar-terms').appendTo($rule);

			if (definitionIndex !== undefined) {
				// for the definition index given:
				var rule = rules.definition[definitionIndex];
				// for each term in this rule:
				_(rule).each(function(term) {
					var $term = $('<div />').addClass('grammar-term').appendTo($terms);
					if (term[0] != ':') {
						$term.addClass('grammar-literal');
					} else if (term[0] == ':') {
						$term.addClass('grammar-type-' + grammar[term].type);
					}

					$term.text(term);
				});
			}

			$rule.draggable({
				cursorAt: { left: 20, top: 20 },
				helper: function() { return $('<div />').addClass('grammar-symbol-helper').attr('grammar-symbol', $rule.attr('grammar-symbol')).text($rule.attr('grammar-symbol')); },
				revert: true, revertDuration: 0, opacity: 0.9, cursor: 'move',
				start: function() {
					self.showDragIndicators(symbol, true);
				},
				stop: function() {
					self.showDragIndicators(symbol, false);
				}
			});
		}, this);

		_(grammar).each(function(rules, symbol) {
			if (rules.definition) {
				_(rules.definition).each(function(def, index) { addRule(rules, symbol, index); });
			} else {
				addRule(rules, symbol);
			}
		}, this);
	},
	
	updateCode: function() {
		var self = this;
		var ready = $('.grammar-area .grammar-symbol.grammar-required').length == 0;
		this.readyText.text(this.programReadyText[ready ? 1 : 0]);

		if (ready) {
			var compiler = new Compiler();
			var code = '';
			$('.grammar-area').find('.grammar-literal, .grammar-input-text, .grammar-symbol').each(function(i, e) {
				var element = $(e);
				if (element.hasClass('grammar-symbol')) {
					code += '/* ' + element.text() + '*/ ';
				} else {
					code += element.text();
				}
			});
			self.codeOutput.html('Compiling and running program...');
			compiler.run(code, function(result) {
				self.codeOutput.html(result);
			});
		} else {
			self.codeOutput.html('Program not ready.');
		}
	},

	programReadyText: ['Your program is not yet ready. The parts of the program with red borders are required for it to run.', 'Your program is a complete program. The result of the program is shown below. You can still make changes to the program.'],
	setupCode: function() {
		this.readyText = $('<div />').addClass('grammar-readiness').text(this.programReadyText[0]).appendTo(this.code);
		this.codeOutput = $('<pre />').addClass('grammar-output').appendTo(this.code);
	}

});
