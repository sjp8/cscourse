
var LessonColumn = Class.extend({

	init: function() {
		this.element = $('<div />').addClass('page-column');
		this.inner = $('<div />').addClass('page-column-inner').appendTo(this.element);
		this._reverse = false;
		this._midpoint = 80;
		this._highlightOpacity = 0.6;

		return this;
	},

	reset: function() {
		this.inner.empty();
	},

	width: function(percentage) { if (percentage == undefined) { return this._width; } this._width = percentage; return this; },
	midpoint: function(percentage) { if (percentage == undefined) { return this._midpoint; } this._midpoint = percentage; return this; },
	height: function(height) { if (height == undefined) { return this._height; } this.element.css('height', height); this._height = height; return this; },
	highlight: function(opacity) { this._highlight = true; if (opacity) { this._highlightOpacity = opacity; } return this; },

	reverse: function() {
		this._reverse = true;
		this.midpoint(25);
		this.element.addClass('reverse-column');
		return this;
	},

	title: function(title) {
		this._title = title;
		this.inner.prepend($('<div />').addClass('page-column-title').text(title));
		return this;
	},

	setPosition: function(onFinish) {
		var self = this;
		if (self._onFinishTemporary) { // currently animating
			self._onFinishTemporary = function() { self.setPosition(onFinish); };
			return;
		}

		self._onFinishTemporary = true;

		// move content up or down with enough space for the new element
		var contentHeight = this.inner.height();

		var visibleHeight = this.element.height() || $(window).height() * .8;
		var midpoint = (this._midpoint / 100) * visibleHeight;

		// sum of height of new elements
		var newElements = this.inner.find('.column-unprocessed');
		var calculateHeight = function(elements) { return _.reduce(newElements.get(), function(memo, e) { return memo + $(e).height(); }, 0); };
		var newContentHeight = calculateHeight(newElements);

		// TODO: scroll or resize sets of elements that cannot fit in the given content area.
		newElements.removeClass('column-unprocessed');

		var negativeMargin = this._reverse
			? -(midpoint + newContentHeight) // make room for new elements by lowering content
			: (contentHeight + newContentHeight) - midpoint;

		// adjust the column contents up or down to create space for new elements
		this._highlight && this.inner.children().animate({ opacity: this._highlightOpacity });
		this.inner.animate({ top: -negativeMargin }, {
			easing: 'swing',
			always: function() {
				if (self._reverse) {
					self.inner.css('top', midpoint); // show elements, and revert temporarily lowered content
				}

				newElements.css('opacity', '').css('color', '');
				newElements.fadeIn();
				onFinish && onFinish();

				_.isFunction(self._onFinishTemporary) && self._onFinishTemporary();
				self._onFinishTemporary = null;
			}
		});

		// draw a horizontal line to delineate new content appearing
		// appear new content
		// remove horizontal line
	},

	addElement: function(lessonElement) {
		lessonElement.insert(this.inner); // insert into a temporary div to run insert code
		var element = lessonElement.element;

		if (this._reverse) {
			element.prependTo(this.inner); // move instead to beginning
		}
		element.hide().addClass('column-unprocessed');
	},

	removeElement: function(element) {
		// todo remove an element for undo
	}


});
