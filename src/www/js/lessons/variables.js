
var VariablesLesson = Lesson.extend({
	setup: function() {

		this.addPage(
			new LessonPage('What is a Variable?')
				.addColumns('half half')

				.addElement(new TextElement().text('Variables are used to store data temporarily. Variables are the "memory" of a program. Imagine that you are working out an algebra problem that it took six or more steps to simplify. You write down each step in the process on paper, to keep track as you work toward the final result.'))
				.addElement(new TextElement().text('Similarly, in a Java program it might be necessary to calculate an answer in multiple steps. In Java, you can keep track of each answer by storing the result of each computation in a variable.'))
				.addElement(new ImageElement().column(1).file('variables_memory_card.png').dim(738, 553))
				.addElement(new TextElement().column(1).text('Above: memory sticks where data is actually stored in a computer.'))
				.onContinue()

				.addElement(new VariableTableElement().column(1)
					.setTitle('Java Variable Table')
					.setVariable('width', '51.0', 'double')
					.setVariable('height', '34.0', 'double')
					.setVariable('depth', '12.0', 'double')
					.setVariable('quantity', '4', 'int')
					.setVariable('description', '"Cabinet"', 'String')
				)
				.addElement(new TextElement().text('Variables are essentially labeled information is used in programs. The table to the right is an example of labeled information. Each variable has three pieces of information: a <i>name</i>, a <i>value</i>, and a <i>type</i>.'))
				.addElement(new ListElement().items('The name is how you refer to the variable.', 'The value is the data associated with the variable.', 'The type of the variable determines what type of value can be stored in it.'))
				.onContinue()

				.addElement(new TextElement().text('The values of variables can be changed. For instance, the <code>quantity</code> variable could be increased by one, to 5.'))
				.addElement(new TextElement().text('New variables can be added at any time. Each new variable appears as a new row in the table.'))

		)

		this.addPage(
			new LessonPage('Declaring, Initializing, Assigning Variables')
				.addColumns('half half')

				.addElement(new TitleElement().text('Declaring a Variable'))
				.addElement(new TextElement().text('Declaring a variable adds it to the variable table. To <i>declare</i> a new variable in Java, at minimum, you need to specify a type and a name for the variable: <pre>double distance;</pre> This will add a variable called distance to the variable table, with a type of double, and double\'s default value: 0.0. The <i>default</i> value is the value given when no value is explicitly set. For integers, the default value is 0.'))
				.addElement(new ImageElement().file('variables_declaration_simple.svg').dim(365, 202))
				.addElement(new VariableTableElement().name('variable_table0').column(1)
					.setTitle('Java Variable Table')
					.setVariable('distance', '0.0', 'double')
				)
				.onContinue()

				.addElement(new TextElement().text('Optionally, you can give the variable a value to start with. This is called <i>declaring</i> (<code>int seconds</code>) and <i>initializing</i> (<code>seconds = 10</code>) in one step: <pre>int seconds = 10;</pre>'))
				.addElement(new ImageElement().file('variables_declaration.svg').dim(365, 202))
				.addElement(new CommandElement().item('variable_table0').commands(function() { this.setVariable('seconds', '10', 'int'); }))

				.onContinue()

				.addElement(new TitleElement().text('Assigning New Values to a Variable'))
				.addElement(new TextElement().text('Variables must first be declared, then values can be assigned. A variable cannot be changed if it is not in the variable table. After the variable has been declared, its value can be changed by <i>assigning</i> a new value: <pre>distance = 52.5;</pre>'))
				.addElement(new CommandElement().item('variable_table0').commands(function() { this.setVariable('distance', '52.5'); }))
				.onContinue()

				.addElement(new TextElement().text('Variables can be assigned to the result of an expression such as a mathematical expression. The expression will be evaluated, and then the final result assigned as the variable\'s new value. <pre>seconds = seconds + 5;\ndouble velocity;\nvelocity = distance / seconds;</pre>'))
				.addElement(new CommandElement().item('variable_table0').commands(function() { this.setVariable('velocity', '3.5', 'double'); this.setVariable('seconds', '15'); }))

				.onContinue()

		)

		this.addPage(
			new LessonPage('Variables Practice')
				.addColumn(new LessonColumn().midpoint(90).width(50))
				.addColumn(new LessonColumn().width(50))

				.addElement(new TextElement().column(1).text('Variable Challenges:'))
				.addElement(new ListElement().column(1).items('Declare a variable called "volume", and calculate it using width, height, and depth.', 'Change description to "Blue Cabinet" without typing the word Cabinet.', 'Increase quantity by 5.', 'Try out variables and calculations of your own.'))
				.addElement(new VariableTableElement().name('variable_table1').column(1)
					.setVariable('width', '51.0', 'double')
					.setVariable('height', '34.0', 'double')
					.setVariable('depth', '12.0', 'double')
					.setVariable('quantity', '4', 'int')
					.setVariable('description', '"Cabinet"', 'String')
					.setTitle('Java Variable Table')
				)

				.addElement(new TextElement().text('Below, you can practice declaring, declaring and initializing, and assigning variables. Start by putting your cursor in the input box. Then, try declaring a new variable. Notice that as you type, the criteria below the input box will tell you what you have done, and what is left to go. When one of the three criteria is met, you are done: press Enter. Try to solve the challenges to the right.'))
				.addElement(new ConsoleElement().name('table_console').hideOutput()
							.onEvaluate(function(code, result, error) {
								var table = this.byName('variable_table1');
								var variables = table.getVariables();
								var console = this;
								console.setError(error);

								if (error) {
									return;
								}

								// is variable declare and init
								var declareAndInit = code.match(/^\s*(double|int|String|boolean)\s+([a-z][A-z0-9_]*)\s*=\s*(.*);/);
								var declareOnly = code.match(/^\s*(double|int|String|boolean)\s+([a-z][A-z0-9_]*)\s*;/);
								var assign = code.match(/^\s*([a-z][a-zA-Z0-9]*)\s*=\s*(.*);\s*$/);

								var evaluateVariable = function(varName, type) {
									console.evaluate('System.out.print(' + varName + ');', function(c2, r2, e2) {
										var v = _.find(variables, function(x) { return x.name == varName; });
										if (type == 'String' || (v && v.type == 'String')) {
											r2 = '"' + r2.replace(/["]/g, '\\"') + '"';
										}
										table.setVariable(varName, r2, type ? type : undefined);
									});
								}
								if (declareAndInit) {
									evaluateVariable(declareAndInit[2], declareAndInit[1]);
								} else if (declareOnly) {
									evaluateVariable(declareOnly[2], declareOnly[1]);
								} else if (assign) {
									evaluateVariable(assign[1])
								}
							})
							.preCode('double width = 51.0; double height = 34.0; double depth = 12.0; int quantity = 4; String description = "Cabinet";')
							.onInputUpdate(function(input) {
								var declareInfo = this.byName('table_declare');
								var initializeInfo = this.byName('table_declare_initialize')
								var assignInfo = this.byName('table_assign');
								var table = this.byName('variable_table1');

								var hasType = input.match(/^\s*(double|int|String|boolean)/);
								var hasName = hasType && input.match(/^\s*(double|int|String|boolean)\s+([a-z][a-zA-Z0-9]*)/);
								var hasIncorrectName = hasType && input.match(/^\s*(double|int|String|boolean)\s+([a-zA-z0-9]+)/);
								var hasAssignmentOperator = hasName && input.match(/^\s*(double|int|String|boolean)\s+([a-zA-Z0-9]+)\s*=/);
								var hasValue = hasAssignmentOperator && input.match(/=\s*([A-z0-9*+-\/"][A-z0-9*+-\/ "]*)/);

								if (hasValue) {
									var value = hasValue[1];
									if (hasType[1] == 'String') {
										var substring = value.substring(1, value.length - 1);
										var validValue = value[0] == '"' ? (value[0] == '"' && value[Math.max(1, value.length - 1)] == '"' && (!substring.match(/["]/g) || (substring.match(/["]/g).length == substring.match(/\\"/g).length))) : true;
										if (!validValue) {
											hasValue = false;
										}
									}
								}

								var hasSemicolon = input.match(/;\s*$/);

								var variables = table.getVariables();
								var notDeclared = hasName && !_.any(variables, function(v) { return v.name == hasName[2]; });

								declareInfo.setInfo('Has type?', hasType ? hasType[1] : 'Not yet', 'check', hasType);
								declareInfo.setInfo('Has name?', hasName ? 'Yes: ' + hasName[2] : (hasType ? (hasIncorrectName ? 'Yes, but incorrect: <del>' + hasIncorrectName[2] + '</del> Variable names start with a lowercase letter (a to z), followed by any letters and numbers, with no spaces or symbols.' : 'Not yet.') : 'Need Type first.'), 'check', hasName);
								declareInfo.setInfo('Has semicolon?', (!hasAssignmentOperator && hasName && hasSemicolon) ? 'Yes. Declaration finished! Press <kbd>Enter</kbd>.' : 'Not yet.', 'check', (!hasAssignmentOperator && hasName && hasSemicolon));
								declareInfo.setInfo('Not already declared?', hasName ? (notDeclared ? 'Yes, not already declared.' : 'Declared, variable name conflict.') : 'n/a', 'check', hasName && notDeclared);

								initializeInfo.setInfo('Has type?', hasType ? 'Yes: ' + hasType[1] : 'Not yet.', 'check', hasType);
								initializeInfo.setInfo('Has name?', hasName ? 'Yes: ' + hasName[2] : (hasType ? (hasIncorrectName ? 'Yes, but incorrect: <del>' + hasIncorrectName[2] + '</del> Variable names start with a lowercase letter (a to z), followed by any letters and numbers, with no spaces or symbols.' : 'Not yet.') : 'Need Type first.'), 'check', hasName);
								initializeInfo.setInfo('Has assignment operator?', hasAssignmentOperator ? 'Yes.' : (hasName ? 'Not yet.' : 'Need Name first.'), 'check', hasAssignmentOperator);
								initializeInfo.setInfo('Has value?', hasValue ? 'Yes: <code>' + value + '</code>': (hasAssignmentOperator ? 'Not yet.' : 'Need Assignment Operator first.'), 'check', hasValue);
								initializeInfo.setInfo('Has semicolon?', (hasValue && hasSemicolon) ? 'Yes. Declare and Initialize finished! Press <kbd>Enter</kbd>.' : (hasValue ? 'Not yet.' : 'Needs Value first.'), 'check', hasValue && hasSemicolon);
								initializeInfo.setInfo('Not already declared?', hasName ? (notDeclared ? 'Yes, not already declared.' : 'Declared, variable name conflict.') : 'n/a', 'check', hasName && notDeclared);

								var hasName = input.match(/^\s*([a-z][a-zA-Z0-9]*)/);
								var hasAssignmentOperator = input.match(/^\s*[a-z][a-zA-Z0-9]*\s*=/);
								var hasValue = hasAssignmentOperator && input.match(/=\s*([A-z0-9*+-\/"][A-z0-9*+-\/ "]*)/);
								var declared = hasName && _.any(variables, function(v) { return v.name == hasName[1]; });

								hasName = hasName ? (_.indexOf(['double', 'int', 'String', 'boolean'], hasName[1]) === -1 && hasName) : null;
								assignInfo.setInfo('Has variable name?', hasName ? hasName[1] : 'Not yet.', 'check', hasName);
								assignInfo.setInfo('Variable exists?', hasName ? (declared ? 'Yes, variable has been declared.' : 'No, variable needs to be declared first.') : 'n/a', 'check', hasName && declared)
								assignInfo.setInfo('Has assignment operator?', hasAssignmentOperator ? 'Yes.' : 'Not yet.', 'check', hasAssignmentOperator);
								assignInfo.setInfo('Has value?', (hasAssignmentOperator && hasValue) ? 'Yes: ' + hasValue[1] : 'Not yet.', 'check', hasValue && hasAssignmentOperator);
								assignInfo.setInfo('Has semicolon?', (hasAssignmentOperator && hasValue && hasSemicolon) ? 'Yes. Assign finished! Press <kbd>Enter</kbd>.' : 'Not yet.', 'check', hasAssignmentOperator && hasValue && hasSemicolon);

							})
				)
				.addElement(new InformationElement().name('table_declare_initialize')
						.setTitle('Declaring and Initializing a Variable')
						.setInfo('Has type?')
						.setInfo('Has name?')
						.setInfo('Not already declared?')
						.setInfo('Has assignment operator?')
						.setInfo('Has value?')
						.setInfo('Has semicolon?')
				)
				.addElement(new InformationElement().name('table_declare')
						.setTitle('Declaring Variable')
						.setInfo('Has type?')
						.setInfo('Has name?')
						.setInfo('Not already declared?')
						.setInfo('Has semicolon?')
				)
				.addElement(new InformationElement().name('table_assign')
						.setTitle('Assigning a value to a variable')
						.setInfo('Has variable name?')
						.setInfo('Variable exists?')
						.setInfo('Has assignment operator?')
						.setInfo('Has value?')
						.setInfo('Has semicolon?')
				)
				.onContinue()
		)

		this.addPage(
			new LessonPage('Variables in Programs')
				.addColumns('main terms')

				.addElement(new TextElement().text('In a program, variables are used within methods. The Variable Table in the previous pages starts empty when entering a method, and when the method is finished, is no longer accessible.'))
				.addElement(new TextElement().text('Even if a method (methodOne) calls another method (methodTwo), methodTwo does not have access to methodOne\'s variables, and methodOne does not have access to methodTwo\'s variables. In programming languages, the concept of separate methods having a separate variable table is called "scope".'))
				.addElement(new TextElement().column(1).text('<b>Scope</b> refers to the area in a program (for example, within a given method) in which a given set of variables can be accessed.'))
				.onContinue()

				.addElement(new TextElement().text('Consider the following example. If the class\'s main method (not shown) calls methodOne, what would the output of the program be?'))
				.addElement(new ImageElement().file('variables_methods_example1.svg').dim(770, 361))
				.onContinue()
				
				.addElement(new TextElement().text('Here are the answers, and program output:'))
				.addElement(new ImageElement().file('variables_methods_example2.svg').dim(766, 423))
				.addElement(new TextElement().text('Program output: <pre>methodOne: a: 33\nmethodTwo: a: 24'))
		)
		
		this.addPage(
			new LessonPage('Variables in a Program Example')
				.addColumns('half half')

				.addElement(
					new ProcessingEditorElement().column(1).name('clock_editor')
						.processingCodeFile('js/processing/variables_clock.pde')
						.javaCodeFile('js/processing/variables_clock_java.txt')
				)
				.addElement(new TextElement().text('To the right is a program in which you can practice using variables in basic calculations of the second, minute, and hour of a clock. By filling in the missing calculation methods, you will make an analog clock function.'))
				.addElement(new ImageElement().file('variables_clock_schema.svg').dim(607, 328))
				.onContinue('Instructions')

				.addElement(new TextElement().text('<b>Instructions:</b> In the <code>calculateSecond</code> method, assign the correct <i>second</i> value to the <code>second</code> variable. This is the number of the second on the clock (from 0 to 59). Use <code>secondOfDay</code> in your calculation. <code>secondOfDay</code> contains the total # of seconds that have passed during the day so far (i.e. at 30 seconds past 7:32pm, the <code>secondOfDay</code> value would be <code>7 * 3600 + 32 * 60 + 30</code>. You may find the modulus and division operators useful in some of these calculations.'))
				.addElement(new TextElement().text('When are ready to test your <code>calculateSecond</code> method, click Run Your Code. If you do not have errors in your code, a clock with a ticking second hand will display. The minute hand and hour hand will not move until you implement <code>calculateMinute</code> and <code>calculateHour</code>. When the second hand is working, move on to the minute and hour hands, until all three hands tick and show the real time.'))
		)

	}

});
