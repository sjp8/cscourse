
var DraftingToolsLesson = Lesson.extend({
	setup: function() {
		this.addPage(
			new LessonPage('Plain Java Editor')
				.addColumns('full width')
				.addElement(new JavaEditorElement().admin())
		)
	}
	
});
