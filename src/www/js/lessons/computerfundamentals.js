
var ComputerFundamentalsLesson = Lesson.extend({
	setup: function() {

		this.addPage(
			new LessonPage('What is a Computer?')
				.addColumns('main terms')

				.addElement(new TextElement().column(1).text('program: A program is a set of instructions for a computer.'))
				.addElement(new TextElement().text('A program is a set of instructions for a computer. Let\'s first understand what a computer is, and how it works.'))

				.onContinue()

				.addElement(new TextElement().column(1).text('computer: A machine that processes information.'))
				.addElement(new TextElement().column(1).text('processor: The part of a computer that runs programs. The programs run by the processor are stored as coded instructions, in memory.'))
				.addElement(new TextElement().column(1).text('memory: The part of a computer that stores all those 0s and 1s you hear about.'))
				.addElement(new TextElement().text('Programs on most computers are run by a processor, which computes information and exchanges it with memory. As an analogy, imagine a robot (called Processor) that harvests a field of numbers (called Memory). These numbers are pieces of a puzzle - useful pieces of information to help the robot do its job. The robot, based on the instructions given to it, harvests numbers from the field, combines them with its internal calculator, then moves the calculated numbers to a new location in the field. The result, if the instructions given to it are correct, is a working program that accomplishes a goal.'))

				.onContinue()

				.addElement(new TextElement().text('<i>Alternative to robot analogy:</i> Programs on most computers are run by a processor, which computes information and exchanges it with memory. As an analogy, imagine that you are in a library. You are doing research for a project, and need to compile the right information to complete it. To do this, you have a list of instructions (a <u>program</u>) on which books are important, and how to extract the best information from them. Your job is equivalent to that of the <u>processor</u> in a computer; you follow the instructions to get the right pieces of data for your project; helpful pieces of information to achieve the final result. The library is equivalent to <u>memory</u> in a computer. It has the information you need, and is designed to make this knowledge accessible. When the correct instructions, the processor, and memory come together, the computer program is a success.'))

				.onContinue()

		);

		this.addPage(
			new LessonPage('What is a Program?')
				.addColumns('main terms')

				// These instructions are written in computer language.
				.addElement(new TextElement().text('A program is a set of instructions that can be understood by a computer. Programming languages follow grammatic rules, just like the English language.'))
				.addElement(new TextElement().text('There are millions of programming languages, each with its own purpose.'))
				.onContinue()

				.addElement(new TextElement().text('assembly language: A language with basic instructions understood by a computer\' processor.').column(1))

				.addElement(new TitleElement().text('How a processor runs programs'))
				.addElement(new TextElement().text('A processor only understands one language: its own. This language is called assembly language, given to the processor through electrically-encoded signals. For instance, the processor below understands 001 to mean "add".'))

				.addElement(new SVGElement().file('processor_wire_instructions.svg').dim(387, 257))
				.addElement(new TextElement().text('<b>Above:</b> A processor detects a code based on electricity in three wires. The wires without any electrical current indicate "0". The wire with an electrical current indicates "1".'))
				.onContinue()

				.addElement(new QuizElement().randomize().question('If a processor only understands one language, then why are there millions of languages? Take a guess:').addAnswer('There are millions of processors, one for each language.', false).addAnswer('Languages must be translated into the processor\'s language.', true))

				.onFinish()
				.onContinue()

				// The processor reads instructions sequentially (that is, in order) from memory, or, an instruction can tell the processor to skip to a different part of the program.
				.addElement(new TextElement().text('Assembly language instructions direct the processor to perform several basic actions.  There are three types of instructions: mathematical operations, memory operations, and program flow operations. <i>Mathematical operations</i> take two numbers and add, subtract, multiply, and divide them, storing the result in memory for later. <i>Memory operations</i> are used to put a number in a specific location in memory, or to retrieve a number that was previously stored. <i>Program flow operations</i> are used to skip to different parts of the program, allowing a program to give different instructions based on the situation.'))

				.addElement(new TextElement().text('<b>Below:</b> A processor can perform three basic types of instructions.'))
				.addElement(new SVGElement().file('processorinstructiontypes.svg').dim(458, 327, 0.7))
				.onContinue()

				.addElement(new TitleElement().text('A "Computer" Game'))
				.addElement(new ProcessingElement().dim(400, 300).file('robot_processor.pde'))
				.addElement(new TextElement().text('<b>Above:</b> Play the part of a program by instructing the processor to do basic tasks.'))
				.addElement(new TextElement().text('<em>Challenge:</em> Calculate as many numbers of the Fibonacci sequence as possible (0, 1, 1, 2, 3, 5, 8...), leaving them in order in memory. Notice that each number in the sequence is the sum of the previous two numbers.'))
				.onContinue()

				.addElement(new TitleElement().text('High-level languages'))
				.addElement(new TextElement().text('compile: to translate a human-readable programming language such as Java into computer-readable assembly language.').column(1))
				.addElement(new TextElement().text('Languages such as Java cannot be understood directly by a processor. They must be <i>compiled</i> into assembly language. This compiled code can be understood and run by the processor.'))

				.addElement(new TextElement().text('high-level language: A programming language that is more abstract than assembly language. Because it does not conform exactly to a processor\'s instructions, it must be translated into assembly language before it can be run by the computer.').column(1))
				.addElement(new TextElement().text('Java is called a high-level language. This means that it is more abstract than assembly language. While assembly language looks like a never-ending algebra problem, Java code uses abstract concepts like "List", "Number", "Word", "FileReader", etc. Compare this to the concepts that a computer processor understands: do math, store number, retrieve number. Java greatly simplifies programming tasks.'))
				.onContinue()

				.addElement(new SVGElement().file('compileassembly.svg').border().dim(505, 242))
				.addElement(new TextElement().text('<b>Above:</b> Java is translated into Assembly Language using a Compiler.'))
		);

		this.addPage(
			new LessonPage('Examples of Programs')
				.addColumn(new LessonColumn().width(100))
				.addElement(new TextElement().text('Programs are used ubiquitously in all fields of knowledge. In chemistry, programs model molecules and reactions. In biology, programs are used to analyze DNA, or to analyze blood and tissue samples to provide diagnosis. In mathematics, programs calculate and model to aid in understanding and calculating problems in mathematical fields such as statistics, geometry, fractals, logic, and algebra. Computer Science and the problems it tries to solve is a part of the field of mathematics.'))
				.onContinue()

				.addElement(new TextElement().text('DNA is a program, because of its ability to create living matter based on its pattern. Organic cells are created by the synthesis of proteins with DNA.'))
				.onContinue()

				.addElement(new TextElement().text('Games are programs that combine sound and moving images in changing environments. Programmed characters are coded to have artificial intelligence. (Basic artificial intelligence, defined as behavioral changes <i>learned</i> from from the environment, is an assignment later in the course.) Basic physics algorithms control gravity, and collision with walls, objects and other characters.'))
				.onContinue()

				.addElement(new TextElement().text('Operating Systems, such as Windows, Linux, and Apple OS, are programs that maintain peaceful relations between the computer hardware (processor and memory), and the hundreds of programs running on the computer. They also provide interfaces (connections) to common hardware such as the keyboard, monitor, USB ports, audio, mouse, networking ports, and printer.'))
				.onContinue()

				.addElement(new TextElement().text('Web Browsers such as Internet Explorer, Google Chrome, Safari, and Firefox are programs that interpret and display web pages. Web pages are written in a collection of languages for various purposes: textual and visual displays, animation, and interactive programs.'))
				.onContinue()

				.addElement(new TextElement().text('Computer programs run systems airplanes, satellites and spacecraft, and throughout technology and machinery. Small computers (unlike personal computers, which have a complex assembly) are pre-coded and combined with other materials and engines to create hovercraft, refrigerators and elevators, and stage lighting.'))
		);

		this.addPage(
			new LessonPage('Quiz - What is a Program?')
				.addColumn(new LessonColumn().width(75).midpoint(80))
				.addColumn(new LessonColumn().width(25).midpoint(60))

				.addElement(new TextElement().text('A program is defined as a set of instructions that can be understood by a computer.').column(1))
				.addElement(
					new QuizElement()
						.multiple()
						.question('Which of the following examples is a program? (Click each example that matches the description.)')
						.addAnswer('Operating Systems such as Windows, Linux, and Apple OS', true, "An operating system is a program that runs and controls other programs, and helps them run consistently on a variety of computers.")
						.addAnswer('Google Chrome', true, "It's a program that runs web pages on the internet.")
						.addAnswer('A screensaver', true, "This program runs animations on the computer screen.")
						.addAnswer('A computer screen', false, "It can be controlled by a program, but is just a piece of furniture on its own.")
						.addAnswer('A calculator', true, "A calculator is a small computer with a program that turns input (numbers and mathematical symbols) into output (the answer). If you answered negatively because you thought of the calculator as a computer instead of a program, you are also correct.")
						.addAnswer('A clock', true, "Digital clocks are run by a very small computer that is built to run only one program... twenty. four. hours. a. day.")
						.addAnswer('A search engine', true, "This program gathers information from the four corners of the Earth, at least the ones with electricity, and summarizes it in ten lines or less.")
				)

				.onFinish()
		);
	}

});
