
var MathematicalExpressionsLesson = Lesson.extend({

	setup: function() {
		this.addPage(
			new LessonPage('Mathematical Expressions')
				.addColumns('main terms')

				.addElement(new TextElement().column(1).text('Order of operations: the order in which parts of mathematical expressions are evaluated.'))
				.addElement(new TextElement().text('Mathematical expressions in Java follow many of the same rules as in regular algebra.'))
				.addElement(new TextElement().text('Order of operations is the same as normal algebra. Parentheses are first, followed by exponents and roots, followed by division and multiplication, followed by addition and subtraction.'))

				.addElement(new TextElement().text('Java provides a set of math common functions such as trigonometric functions (sine, cosine, tangent, cotangent, secant, cosecant), absolute value, square root, logarithms, and exponents. Java also provides some common constants such as pi, and e.'))
				.addElement(new ImageElement().file('math_equation1.svg').dim(731, 152))
				.onContinue()

				.addElement(new TextElement().column(1).text('Evaluate: to compute the result of.'))
				.addElement(new TextElement().column(1).text('Integer: a whole number. A number that can be written without a fraction or decimal.'))
				.addElement(new TextElement().column(1).text('Rational Number: A number that can be written as a fraction of two whole numbers.'))
				.addElement(new TextElement().text('There are several major differences between how Java evaluates expressions, and common mathematical rules.'))

				.addElement(new TextElement().text('<b>Decimal numbers are called "doubles" in Java.</b> If you were curious, the word "double" is taken from the method by which decimal numbers are stored in computers: "double precision floating-point format", where each number takes <i>two</i> adjacent locations in memory (64 bits).'))
				.addElement(new TextElement().text('<b>Integers.</b> In Java, when two integers are divided, the result is always an integer. Consider <code>5 / 2</code>. You would expect the result to be 2.5. However, in Java, because the result of dividing two integers must be an integer, the result will be 2. When dividing <code>-5 / 2</code>, the result is -2. The formula for dividing integers in Java is: divide like in normal math, then remove decimals (numbers to the right of the decimal point).'))
				.addElement(new TextElement().text('<b>Combining Integers and Decimals.</b> When adding, subtracting, multiplying, or dividing an integer with a decimal number, the result is also a decimal number.'))
				.addElement(new TextElement().text('<b>Casting to a different type</b> You can cast an integer to a double (5 becomes 5.0), and a double to an integer (3.9 becomes 3).'))
				.onContinue()

		)

		this.addPage(
			new LessonPage('Operator Meanings')
				.addColumns('full width')

				.addElement(new TitleElement().text('Addition, Subtraction'))
				.addElement(new ListElement()
						.items(
							'Negative sign: <code>1 + -1</code> is 0.',
							'Adding and subtracting integers: <code>1 + 2 - 3 + 4 - 5 + 6</code> is 5. The expression is evaluated from left to right.',
							'Adding and subtracting doubles: <code>1.0 + 1.5 + 1.7 - 7.1 + 5.1 + 0.1</code> is 2.3',
							'Mixing integers and doubles: <code>1 + 3.5 - 2.2</code> is 4.5 - 2.2 is 2.3. Combining an integer with a double will always result in a double.'
						)
					)
				.onContinue()
				.addElement(new TitleElement().text('Multiplication, Division'))
				.addElement(new ListElement()
						.items(
							'Integers: <code>6 * 2 / 3</code> is (12) / 3 is 4. When there is multiplication and division, they are done from left to right.',
							'Doubles: <code>6.6 / 3.0 * 2.0 is 2.2 * 2.0 is 4.4',
							'Mixing integers and doubles: <code>6 / 3 * 2.5</code> is 2 * 2.5 is 5.0.',
							'Dividing integers: <code>10 / 2</code> is 5, <code>11 / 2</code> is 5 (5.5 without decimals), <code>-10 / 3</code> is -3 (-3.3 without decimals).',
							'Mixing integers and doubles, with integer division: <code>7 / 3 * 2.2</code>. First divide 7 / 3 (it is 2, which is the result of removing decimals from 2.3333). Then, multiply the result by 2.2: 2 * 2.2 is 4.4 (4.4 is a decimal number).',
							'Mixing with addition/subtraction: <code>1 - 2 * 5.9</code> is 1 - (2 * 5.9), because multiplication is performed before addition. 1 - (2 * 5.9) is 1 - (11.8) is -10.8',
							'Dividing by zero: <code>5 / 0</code> is an error in Java. This is because in math, no number can be multiplied by 0 to get 5. So is <code>0 / 0</code>, because any number can be multiplied by zero to get zero.'
						)
					)
				.onContinue()
				.addElement(new TitleElement().text('Modulus'))
				.addElement(new TextElement().text('Modulus (%) is the remainder operation. It gives what is left after dividing as much of the numerator as possible into the denominator.'))
				.addElement(new TextElement().text('Method 1: <code>3 % 2</code>. How many times can 2 go entirely into 3? Only once. What is left (in other words, how far is there left to go) after 2 has gone into 3? 1 more will get you to 3. Therefore the remainder is 1.'))
				.addElement(new TextElement().text('Method 2: <code>3 % 2</code>. 3 / 2 in regular math is 2.5. Multiply the decimal (0.5) by the denominator (2) to get the remainder: 1.'))
				.addElement(new ListElement()
						.items(
							'<code>5 % 2</code>. How many times does 2 go into 5? 5 / 2 is 2.5, so 2 goes into 5 twice, with 0.5 (of 2) remaining. 0.5 * 2 is the remainder, which is 1.',
							'Remainder of doubles: <code>5.3 % 2.1</code> 2.1 goes into 5.3 twice (2.1 * 2 is 4.2). 5.3 - 4.2 is left, which is 1.1. Therefore 1.1 is the remainder.',
							'<code>-5 % 2</code> is -1. <code>5 % -2</code> is 1. The sign of the numerator determines the sign of the result of a modulus expression. Therefore <code>5 % -2</code> and <code>5 % 2</code> are the same.'
						)
					)
				.addElement(new TextElement().text('You can use modulus to determine if a number is odd or even. If the number % 2 is equal to 0, then the number is even. Otherwise, the number is odd.'))
				.onContinue()
				
				.addElement(new TitleElement().text('Parentheses'))
				.addElement(new TextElement().text('Parentheses are used to group expressions, thereby saying that certain expressions should be evaluated first.'))
				.addElement(new TextElement().text('For instance, <code>(2 - 1) * 5</code> is (1) * 5 is 5. Without the parentheses, 1 * 5 would be evaluated first, and the result would be 2 - 5, which is -3.'))
				.onContinue()
				
				.addElement(new TitleElement().text('Basic Math Functions'))
				.addElement(new TextElement().text('The other common operations, such as exponents and roots, are accomplished through the Math class.'))
				.addElement(new ListElement()
						.items(
							'Exponents: <code>Math.pow(2, 3)</code> is 2 to the power of 3. Math.pow(3 - 1 * 2, 5.5 - 3.5) is 4 to the power of 2.0. The base and exponent are each calculated (to be 4 and 2.0), and then finally the power is calculated: 4 ^ 2.0 which is 16.0',
							'Square root: <code>Math.sqrt(4)</code> the square root of 4. You can also use <code>Math.pow(4, 1/2)</code>, or 4 to the power of 1/2, to calculate square root, and <code>Math.pow(4, 1/3)</code> to calculate cubed root.',
							'Trigonometry: <code>Math.sin(Math.PI / 2)</code> calcualtes the sine of &pi; / 2, or 90 degrees. It takes its values in radians. To calculate degrees, remember that 2 * &pi; radians is 360 degrees. Therefore X radians is 360 / (2 * &pi;), or 180 / &pi;. The other functions are Math.cos for cosine, and Math.tan for tangent.'
						)
					)
				.onContinue()
				
				.addElement(new TitleElement().text('Type Casting'))
				.addElement(new TextElement().text('Sometimes, because of the effects of multiplying combinations of decimal points (doubles) and integers, you will want to change the type of parts of your equation.'))
				.addElement(new TextElement().text('Use <code>(int)5.5</code> to remove the decimal point from 5.5, making it an integer. Use <code>(double)7</code> to convert 7 to 7.0, adding a decimal point, making it a double.'))
				.addElement(new ListElement().items(
					'For instance, imagine that you had two integers, 15 and 2, and you wanted to make sure that the result of the division had a decimal point. You would need to convert one of the two numbers to a double to achieve this effect (if one of the number is a double, the result will also be a double). <code>(double)15 / 2</code> is 15.0 / 2, which is 7.5.',
					'Casting the result of an expression to an integer: <code>(int)(5.5 * 11)</code> is <code>(int)(60.5)</code>, which is 60. The type cast, (int), is applied to the content of the parentheses as a whole.'
				))
		);

		this.addPage(
			new LessonPage('Order of Operations')
				.addColumn(new LessonColumn().width(50))
				.addColumn(new LessonColumn().width(50))

				.addElement(new TextElement().text('Order of operations in Java, from first to last, is:'))
				.addElement(
					new TableElement()
						.headers('Operators', 'Order', 'Function')
						.row('( ), Math.pow, Math.sqrt, Math.round', '1st', 'Math functions, parentheses')
						.row('(int), (double)', '2nd', 'Casting to int or double. E.g. (int)5.5 is 5')
						.row('*, /, %', '3rd', 'Multiplication, Division, Modulus (Remainder)')
						.row('+, -', '4th', 'Addition, Subtraction')
				)
		)
		
		this.addPage(
			new LessonPage('Java Math Practice')
				.addColumns('half half')
				.addElement(new TextElement().text('(double)1\n-(int)1.1\n-1 + 3 - 4\n1 + 2 * 3 - 4 % 5\nMath.pow(Math.sqrt(100), 2)\n(1 + 1) * 40 % 40\n5 * (3 / 3.0) + (3 - 4) * 10 % 2\nMath.pow(2, (int)(5 % 3))\nMath.sqrt(5 * 5 * 5 * 5) / 5 - (int)-3.5'))
		)
	}

});
