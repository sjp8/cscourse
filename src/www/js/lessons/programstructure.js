
var ProgramStructureLesson = Lesson.extend({
	setup: function() {
		this.addPage(
			new LessonPage('Java Program Structure')
				.addColumn(new LessonColumn().width(50).highlight(0.3))
				.addColumn(new LessonColumn().width(50))

				.addElement(new TitleElement().text('Steps to writing a program'))
				.addElement(new TextElement().text('<ol><li>Understand and research the problem.</li><li>When you know enough to be comfortable reasoning about how to solve the problem, think about and create an algorithm (a process or recipe) to solve the problem.</li><li>Separate the overall task into steps, or sub-tasks.</li><li>Write code for each smaller step, and, combine the steps to complete the overall program.</li></ol> How important it is to introduce the concept of research and algorithms this early, since the lesson is about structure. But the idea of research - algorithm - coding is a powerful concept because it is the fundamental process for creating any program.'))

				.onContinue()

				.addElement(new SVGElement().column(1).file('programstructure_elevator.svg').dim(466, 621))
				.addElement(new TitleElement().text('A basic program example'))
				.addElement(new TextElement().text('Java Programs are organized into a structure.'))
				.addElement(new TextElement().text('The program to the right (described in English) describes how to run an elevator. Let\'s take a look at its components.'))
				.addElement(new TextElement().text('<b>The Program:</b> The green block represents the program as a whole, including its overall solution, and smaller sub-tasks.'))
				.addElement(new TextElement().text('<b>The Overall Solution:</b> The orange block represents the algorithm, or process, used to make the elevator work. Notice that it uses both the sub tasks: moving/stopping the elevator, and opening/closing the door.'))
				.addElement(new TextElement().text('<b>Sub Tasks:</b> The red blocks represent smaller tasks. When considering an overall solution to operating an elevator, it is easier to think "Move Elevator", than it is to think "activate pulley with 746 watts of power for 15 seconds".'))
				.onContinue()

				.addElement(new TextElement().text('Using sub-tasks (these are usually called "methods" in Java) is extremely helpful when writing a program.'))
				.addElement(new TextElement().text('<b>Thinking process</b> It is always helpful to think about the problem beforehand, and come up with the solution without coding anything. In the example to the right, the "Overall Solution" is written so that the general idea of the algorithm (i.e. the step-by-step process) with the elevator operates is very simple. It is written in plain English. The Java equivalent is equally simple, and would read like this: <code>waitForButton(); closeDoor(); moveElevator(); stopElevator(); openDoor();</code>.'))
				.addElement(new TextElement().text('After the algorithm is defined in Java, the rest of the program can be completed by filling in the details of each sub-task ("closeDoor" means "activate the door pulley for 2 seconds", etc.). After the overall solution and sub-tasks are both finished, the program as a whole will work.'))
				.onContinue()

				.addElement(new TextElement().text('Let\'s take a look at the words Java uses to describe programs. (See the white text in the Elevator program to the right.)'))
				.addElement(new TextElement().text('<b>class</b> In Java, a <i>class</i> contains a program. It is named after the program\'s purpose or description (e.g. "Elevator").'))
				.addElement(new TextElement().text('<b>method</b> In Java, <i>method</i> is the word for a process or algorithm. It is usually named after the process it defines (e.g. "moveElevator"). Methods are called "Functions" and "Routines" in other programming languages.'))
				.addElement(new TextElement().text('<b>main method</b> In Java, the <i>main method</i> contains the process for the overall solution. The main method is what runs when the program starts.'))
		);

		this.addPage(
			new LessonPage('Program Structure - Quiz')
				.addColumns('full width')
				.addElement(new QuizElement().randomize()
					.question('What is the first step to writing a program?')
					.addAnswer('Understand and research the problem.', true, 'In order to prepare for writing the algorithm, or the process by which the problem can be solved, it is necessary to first understand the goals the program needs to accomplish, and brainstorm or outline possible solutions for the most important parts of the program.')
					.addAnswer('Draft an algorithm that solves the problem.', null, 'An algorithm is a recipe to solve a problem. While this is step #2 in "Steps to writing a program", it can in a few situations be necessary to help wrap your mind around the problem.')
					.addAnswer('Begin writing code.', null, 'Only the daring and intrepid go into a problem blind, and often a lack of understanding translates into wasted time coding without focus. Sometimes it is necessary to understand the problem by writing and testing out code.')
					.explanation('Writing a program can be complex, and the steps to doing so practically can be different from person to person. But for any case, understanding the content, purpose, and a solution to the problem is necessary to completing it.')
				)
				.onContinue()

				.addElement(new QuizElement()
					.question('What is the difference between a method and a main method? (Select all answers that are true.)')
					.addAnswer('The main method contains the code that runs when the program first starts.', false)
					.addAnswer('The main method usually describes the overall program, whereas other methods can contain specialized sub-tasks.', false)
					.addAnswer('There can only be one main method, but there is no limit to the number of normal methods.', false)
					.addAnswer('All of the above', true)
				)

				.onContinue()

				.addElement(new QuizElement()
					.question('What are the advantages of using methods in a program?')
					.addAnswer('Methods can be re-used.', false, 'This is true. Often a method is written once, and used elsewhere more than once.')
					.addAnswer('Methods save space.', false, 'This is true. If a method summarizes 10 lines of code, those 10 lines do not have to be repeated in the program. Calling the method suffices to run those 10 lines of code.')
					.addAnswer('Methods help summarize a process so that the process can be thought of simply.', false, 'When a complex function can be summarized into a few words, the rest of the program is made easier.')
					.addAnswer('Methods help summarize the program as a whole.', false, 'Seeing a program with its functions separated into methods is much cleaner than seeing a program written completely in one method.')
					.addAnswer('All of the above', true)
				)
				.onContinue()
		);

		this.addPage(
			new LessonPage('Anatomy of a Program')
				.addColumns('full width')
				.addElement(new GrammarGameElement().grammar(this.firstProgramGrammar))
		)

		this.addPage(
			new LessonPage('Example Java Program')
				.addColumn(new LessonColumn().width(30))
				.addColumn(new LessonColumn().width(70))

				// introduction to RocketDrawer.
				.addElement(new TextElement().text('You have just learned how to write a basic Java class, use a main method, and write new methods. You will now use these basic skills to write a program of your own.'))
				.addElement(new ImageElement().file('rocket_example_view.png').dim(338, 142))
				.addElement(new TextElement().text('First, we will introduce the Rocket Drawer, and teach you how to use three methods that will allow you to draw geometric figures by moving a rocket that traces black lines along its path. Click Continue to start the lesson. The program will appear letter-by-letter in the Code Editor to the right. Click "Skip" under the editor if you want to see the whole lesson at once.'))
				.onContinue()

				.addElement(
					new ProcessingEditorElement().column(1).name('rocket_editor')
						.processingCodeFile('js/processing/rocket_directions.pde')
						.javaCode('class RocketDrawer {\n    public static void main(String[] args) {\n        \n    }\n}')
				)

				.addElement(new EditorPlaybackElement().editor('rocket_editor').file('rocket_basics.txt'))
				.onFinish()
				.addElement(new TextElement().text('Click Continue when you are ready to start the next stage of the tutorial.'))
				.onContinue()

				.addElement(
					new ProcessingEditorElement().column(1).name('rocket_editor2')
						.processingCodeFile('js/processing/rocket_directions.pde')
						.javaCode('class RocketDrawer {\n    public static void main(String[] args) {\n        \n    }\n}')
				)
				.addElement(new TextElement().text('<b>Next:</b> You will learn how use the <code>turnLeft();</code>, <code>turnRight();</code>, and <code>moveForward();</code> commands to draw a complete circle.'))
				.onContinue('Start Circle Tutorial')
				.addElement(new EditorPlaybackElement().editor('rocket_editor2').file('rocket_circle.txt'))
				.onFinish()
				.addElement(new TextElement().text('<b>Next:</b> You will learn how to use methods to draw complex patterns with the rocket drawer.'))
				.onContinue('Start Methods Tutorial')
				.addElement(new EditorPlaybackElement().editor('rocket_editor2').file('rocket_methods.txt'))
				.onFinish()
				.addElement(new TextElement().text('<b>Finished.</b> You can return to this tutorial at any time by clicking "Rocket Drawer" on the home page. Click Continue to move to the next page.'))
		);

	},

	// grammar for Anatomy of a Program
	firstProgramGrammar: {
		":Class": {
			"type": "block",
			"required": true,
			"help": "A class contains a collection of instructions for the computer, organized into sections called methods. The entry point for the program is called the Main method.",
			"definition": [
				["class ", ":ClassName", " {", ":MainMethod", ":Method", "}"]
			]
		},
		":ClassName": {
			"type": "input",
			"required": true,
			"pattern": /^[A-Z][A-z]*$/,
			"help": "A class name starts with a capital letter, with the letter of each word capitalized.. e.g. MyClass. Spaces are not allowed. The class name describes the purpose of the class (e.g. PrintingProgram, FirstProgram)."
		},
		":MainMethod": {
			"type": "block",
			"required": true,
			"help": "The main method is a special type of method. It is where the program starts.",
			"definition": [
				["public ", "static ", "void ", "main ", "(String[] args) ", "{", ":Instructions", "}"]
			]
		},
		":Method": {
			"type": "block",
			"multiple": true,
			"required": false,
			"help": "A method is a named set of code that can be called from other methods.",
			"definition": [
				["static ", "void ", ":MethodName", "(", ") ", "{ ", ":Instructions", "}"]
			]
		},
		":MethodName": {
			"type": "input",
			"required": true,
			"pattern": /^[a-z][A-z]*$/,
			"help": "A method name starts with a lowercase letter, with the letter of each following word capitalized.. e.g. myMethodName. Spaces are not allowed. The method name describes its purpose. It is usually a verb, whereas the class is a noun."
		},
		":Instructions": {
			"type": "block",
			"help": [
				"This instruction prints words made up of numbers and letters (called a String) to the console.",
				"This instruction 'calls' a method that you have written (write a method by dragging a :Method to the left). Calling a method means telling it to run its instructions."
			],
			"multiple": true,
			"definition": [
				["System.out.println", "(", ":String", ")", ";"],
				[":MethodName", "()", ";"]
			]
		},
		":String": {
			"required": true,
			"help": "A string is a series of letters and numbers. In a program, strings are enclosed (at the start and end) with \".",
			"type": "inline",
			"definition": [
				['"', ":Value", '"']
			]
		},
		":Value": {
			"required": true,
			"help": "Enter any letters and numbers. The letters you type will be enclosed by a \" on either side, to denote a String.",
			"type": "input",
			"pattern": /[A-z0-9&%$#@!]*/
		}
	}

});