
var BooleanExpressionsLesson = Lesson.extend({
	setup: function() {
		this.addPage(
			new LessonPage('Introduction to Booleans', 'general')
				.addColumn(new LessonColumn().width(70))
				.addColumn(new LessonColumn().width(30))

				.addElement(new TextElement().text('Boolean logic is fundamental to computer science, and deals with two possibilities: true and false. Boolean expressions are logical expressions that can be either true or false.'))
				.addElement(new TextElement().text('For instance, <blockquote>"The sky\'s color is blue"</blockquote> This is a simple boolean statement that is <i>true</i> during the daytime, if the sky is clear. When the sky is violet or dark, the statement is <i>false</i>. This statement might be used to test whether or not to send a satellite into space. That way, the engineers can see what they are doing during the launch, and the statement also excludes all types of bad weather.'))
				.onContinue()

				.addElement(new TextElement().text('The following boolean statement could be true or false based on the circumstances: <blockquote>"It is daytime, or it is nighttime and it is not raining."</blockquote> When it is raining, the statement is false; when the weather is clear, the statement is true; when the weather is cloudy, the statement is also true. In all other cases, the statement is false (for instance, when it is nighttime and raining. To the right is a Venn Diagram with the possible scenarios, and for which scenarios the boolean statement is true (green):'))
				.addElement(new ImageElement().file('booleans_example2.svg').dim(309, 265).column(1))
				.onContinue()
				
				.addElement(new TextElement().text('Here are some examples of how various boolean expressions are represented by Venn Diagrams: The boolean statement "Day and Night" is represented by the area in which the Day and Night circles overlap (i.e. intersect). The boolean statement "Day and Night and Raining" is represented by the area in which the Day, Night, and Raining circles intersect. The boolean statement "Day Or Night" is represented by the combined area of the Day circle and the night circle. For the boolean statement "true", the Venn Diagram would be fully green regardless of circles.'))	
				.onContinue()
				.addElement(new QuizElement().question('For the statement, "It is raining, and it is not night", which area of a Venn Diagram would be colored green (true)?').addAnswer('The raining circle, except for the area where it overlaps with night.', true).addAnswer('The whole area, including outside the circles, except for the night circle.', false).addAnswer('The area where night and raining intersect.', false))
		);

		this.addPage(
			new LessonPage('Java Booleans', 'java')
				.addColumn(new LessonColumn().width(70))
				.addColumn(new LessonColumn().width(30))

				.addElement(new TextElement().text('Boolean expressions in Java are comprised of these basic concepts:'))
				.addElement(new ListElement().items(
					'<b>true and false</b>. <code>true</code> is the result when something is true, <code>false</code> is the result when something is not true. For example, it is true that the number &pi; is equal to circumference of any circle, divided by its diameter. It is false that white is the same color as black. True is not false, and false is not true.',
					'<b>operators and, not, or</b>. These are the fundamental boolean operators. In Java, "and" is <code>&&</code>, "or" is <code>||</code>, and "not" is <code>!</code>. To the right are the Venn Diagrams for <code>a && b</code>, <code>a || b</code>, and <code>!a</code>',
					'<b>variables</b>. Variables are used, along with comparisons, and boolean operators, to create a full boolean expression. In the examples above, "Raining" is a variable.',
					'<b>math comparisons &lt;, &gt;, ==, !=, &lt;=, &gt;=</b> - less than, greater than, equal, less than or equal to, and greater than or equal to - are the comparison operators.',
					'<b>general comparisons</b> - <code>==</code> and <code>!=</code> can be used to compare booleans, or numbers.'
				))
				
				.addElement(new ImageElement().file('booleans_example3.svg').dim(417, 99).column(1))

				.onContinue()

				.addElement(new TextElement().text('Here is a <i>truth table</i> with the values of some basic boolean expressions. (0 represents false, and 1 represents true. You may also see true written as T and false written as F.) To read the table, take one row and read it from left to right. If <code>A</code> is 1 and <code>B</code> is 0, what is the value of <code>!(A || B)</code>? Find the row where <code>A</code> is 1 and <code>B</code> is 0, then move over to the <code>!(A || B)</code> column: there is your answer.').hidden('The answer is <code>0</code>. See the third row, under the <code>!(A || B)</code> column header.', 'Show Answer'))
				.addElement(
					new TableElement()
						.headers(	'A', 'B', 'A && B', 'A || B', '!A', '!(A || B)', '!A || B')
						.row(		'0', '0', '0', 		'0',		'1',	'1',		'1')
						.row(		'0', '1', '0', 		'1',		'1',	'0',		'1')
						.row(		'1', '0', '0', 		'1',		'0',	'0',		'0')
						.row(		'1', '1', '1', 		'1',		'0',	'0',		'1')
				)

				.onContinue()
				.addElement(new TextElement().text('Here are some basic boolean expressions represented by Venn Diagrams.'))
				.addElement(new ImageElement().file('booleans_example1.svg').dim(612, 639))
				
				.onContinue()
				.addElement(new TitleElement().text('Short-circuiting OR'))
				.addElement(new TextElement().text('When Java is evaluating an OR expression such as <code>1 == 1 || 5 == 5</code>, it only needs to know that one term of the OR expression is true. In other words, if the <code>1 == 1</code> term is true, then the expression as a whole is true. Java evaluates OR expressions from left to right. Because only <i>one</i> of the terms needs to be true for the OR statement to be true, Java will stop once it finds the first true term in the expression. If all the terms are false, then Java would have to evaluate all the terms, then finally return false. In the example above, Java would stop after evaluating 1 == 1 (which is true).'))
				.addElement(new TextElement().text('This feature of Java is called <i>short-circuit evaluation</i>.'))
				.onContinue('Short-Circuit Diagram')
				.addElement(new TitleElement().text('Short-circuit example'))
				.addElement(new ImageElement().file('booleans_short_circuit.svg').dim(540, 433))
				
				
		);

		this.addPage(
			new LessonPage('Boolean Practice', 'practice')
				.addColumns('half half')

				.addElement(
					new VariableTableElement().column(1)
						.setVariable('a', 'boolean', 'false')
						.setVariable('b', 'int', '5')
						.setVariable('c', 'double', '4.3')
				)

				.addElement(new TextElement().text('Refer to the variable table to answer the following quiz questions. If necessary, use the console to evaluate parts of expressions, or to check your answers. Typing out expressions, and seeing the answers, reinforces sensory and experiential learning, and helps to memorize concepts. For example, input: <code>!a && c > 10.0</code>'))
				.addElement(new ConsoleElement().preCode('boolean a = false; int b = 5; double c = 4.3;').surround('System.out.print(<%= value %>);').column(1))

				.addElement(
					new QuizElement()
						.question('Suppose that variable <code>a</code> is set to 9, and <code>b</code> to true. What is the result of the boolean expression: <code>b &lt;= 9 && a != a</code>? (<code>&&</code> means "and").')
						.addAnswer('false', true, '"b is less than or equal to 9" is a true expression because <code>b</code> is actually less than or equal to 9. <code>a != a</code> is the same as <code>true != true</code>, which evaluates to <code>false</code>. As both expressions need to be true in order for the whole expression to be true, the expression as a whole is false.')
						.addAnswer('true', false)
				)
				.onFinish()
				.onContinue()

				.addElement(
					new QuizElement().question('<code>!a || (b &gt; c && c &lt; b)</code>').addAnswer('true', true).addAnswer('false', false).explanation('There are two terms in the expression. Only one has to be true in order for the full expression to be true. <code>!a</code> seems to be the simplest term, and it evaluates to <code>true</code>. Therefore the answer is true. If you wanted to evaluate the term in parentheses, know that <code>b &gt; c</code> and <code>c &lt; b</code> are the same thing, and both are true.')
				)
				.onFinish()
				.onContinue()

				.addElement(
					new QuizElement().question('Which has the highest precedence in Java?').addAnswer('Math operators such as +', true).addAnswer('Math Comparison operators such as &gt;=', false).addAnswer('Boolean operators such as ||', false).explanation('Math is evaluated first. For example, look at the expression <code>b + 5 &gt; 8 || false</code>. <code>b + 5 &gt; 8</code> is a boolean expression, but you need to know the value of <code>a + 5</code> before its truth can be determined by the <code>&gt;</code> operator. Finally, the <code>||</code> operator determines if either value is true, and if so, evaluates to <code>true</code>, otherwise evaluates to <code>false</code>.')
				)
				.onFinish()
				.onContinue()

				.addElement(
					new QuizElement().question('<code>true || false && false</code>').addAnswer('true', true).addAnswer('false', false).explanation('The <code>&&</code> operator is always evaluated before <code>||</code>, just as <code>*</code> is evaluated before <code>+</code>.')
				)
				.onFinish()
				.onContinue()

				.addElement(
					new QuizElement().question('<code>((!a && b != 0) || (int)(c + b) == 10) && (a || !a) && !false</code>').addAnswer('true', true).addAnswer('false', false).explanation('<code>(int)</code> casts a number to an integer, removing its decimal. <code>c + b</code> is 9.3 and <code>(int)(c + b)</code> is therefore 9. <code>a || !a</code> is always true; given the meaning of <code>||</code>. <code>!false</code> evaluates to true. Here are the simplified steps to solving the problem:<ul><li>The original expression: <code>((!a && b != 0) || (int)(c + b) == 10) && (a || !a) && !false</code></li><li>The first parentheses group: <code>(<b>(true && true)</b> || <b>9</b> == 10) && (a || !a) && !false</code></li><li><code>((<b>true</b>) || <b>false</b>) && (a || !a) && !false</code></li><li><code>(<b>true</b>) && (a || !a) && !false</code></li><li>The second parentheses group: <code>(true) && (<b>true</b>) && !false</code></li><li>The third term: <code>(true) && (true) && <b>true</b></code></li><li>The first And: <code><b>true</b> && true</code></li><li>The second And: <code><b>true</b></code></li></ul>')
				)
				.onFinish()
				.onContinue()
				
				.addElement(
					new QuizElement().question('<code>!(b - 5 == 0 || !(true || false && a - b > 0 || !false))</code>').addAnswer('true', false).addAnswer('false', true).explanation('Simplifying the terms one at a time: <code>b - 5 == 0</code> is <code>5 - 5 == 0</code> is <code>0 == 0</code> is <code>true</code>. Because the first term of the <code>||</code> is true, the statement as a whole is true (if one or both Or terms are true, the whole expression is true). Therefore <code>!(true || false && a... etc</code> does not have to be evaluated. Because the whole expression is negated by Not (!), the final answer is false.')
				)

		);
	}
});














