
var StringsLesson = Lesson.extend({
	setup: function() {

		this.addPage(
			new LessonPage('Strings')
				.addColumns('main terms')
				.addElement(new TextElement().column(1).text('A <b>string</b> is a sequence of any number of letters, numbers, and symbols.'))
				.addElement(new TextElement().text('Strings are used to store letters, numbers, and symbols. Any of the words you read on a computer are "strings" behind the scenes.'))
				.onContinue()

				.addElement(new TextElement().text('Strings are sequences of any number of letters, numbers, and symbols. A single letter in the sequence is called a character.'))
				.addElement(new ImageElement().file('strings_sequence.svg').dim(406, 203))
				.onContinue()
				
				.addElement(new TextElement().text('In Java, the same string appears like this: <blockquote class="code code-large">"Wo37#="</blockquote>'))
				.addElement(new TextElement().text('Here are more examples of strings:<blockquote class="code">"35"<br />"thirty five"<br />"Mixtures of !@#$%^&*()_- and 1234567890 and abcdefghijklmnopqrstuvwxyz"<br />"The quick brown fox jumped over the lazy brown dog."</blockquote>'))
				
				.onContinue()
				
				.addElement(new TextElement().text('You can glue strings together to make new strings. This is called concatenation, defined as "linking together in a chain". Here is an example of a concatenated string:<blockquote class="code">"ABC" + "def" + "GHI"</blockquote> creates the string: <blockquote class="code">"ABCdefGHI"</blockquote>'))
				.onContinue()

				.addElement(new TextElement().text('Other than letters, numbers, and symbols, there are several commonly used special characters:'))
				.addElement(new TableElement()
					.headers('Symbol', "Java String", 'Description')
					.row('New Line', '<code>"\\n"</code>', 'Represents a new line character. A new line character is like pressing the Enter key, it moves to a new line and then continues. For example, <code>"abc\\nxyz"</code> represents a string with two lines, the first line with <i>abc</i>, the second line with <i>xyz</i>.')
					.row('Tab', '<code>"\\t"</code>', 'Represents a tab character.')
					.row('Double Quote', '<code>"\\""</code>', 'Represents a double quote character. Because double quotes are used to start and end strings, to use a double quote inside a string, it must be designated as a special character by typing <code>\\"</code>.')
					.row('Backslash', '<code>"\\\\"</code>', 'Represents a backslash character. A single backslash (before n, t, and ") is used to designate special characters. To use a backslash as a regular character, it must be written as <code>"\\\\"</code>')
				)
				.onContinue()

				.addElement(new TextElement().text('When you use the \\ character to designate a special character, this is called "escaping". The special character, <code>\\</code>, together with the next letter (e.g. <code>\\n</code>), is called an "escape sequence".'))
				.addElement(new TextElement().column(1).text('An <b>escape sequence</b> is a string of two or more characters which, together, represent a special character. (e.g. <code>\\n</code>, <code>\\t</code>, and <code>\\\\</code>'))

				.addElement(new TextElement().text(
					'Here are some examples of strings that use escape sequences: <blockquote class="code">' +
					'"To Whom It May Concern:\\n\\t&lt;Indented Introduction Goes Here&gt;"<br />' +
					'"\\"To be, or not to be?\\"\\n\\t- Hamlet"<br />' +
					'"\\\\\\\\__//"<br />'
				))
				.onContinue()

		);

		this.addPage(
			new LessonPage('Practicing Strings')
				.addColumn(new LessonColumn().width(50))
				.addColumn(new LessonColumn().width(50))

				.addElement(new ConsoleElement().surround('System.out.print(<%= value %>);').name('string_console').column(1))
				.addElement(new TextElement().text('The area to the right is called a Java Console. It provides a way to evaluate Java expressions without having to code a class or main method. To use the Console, type a Java expression, such as 5*5, or "this is a string", and press Enter.'))
				.onContinue('Start Practicing Strings')

				.addElement(new TextElement().text('Type any string, making sure to enclose it in double quotes, then press Enter.').hidden('Hint: Enter <code>"your words go here"</code>', 'Hint'))
				.addElement(new ConsoleExpectElement().item('string_console').expect(true, function(v) { try { v = eval(v); return _.isString(v); } catch (ex) { return false; } }, 'Correct.'))
				.onFinish()	
				.addElement(new TextElement().text('Type a string that contains a double quotation mark followed by a single quotation mark.').hidden('Hint: Enter <code>"\\"\'"</code>', 'Hint'))
				.addElement(new ConsoleExpectElement().item('string_console').expect(false, '"\''))
				.onFinish()
				.addElement(new TextElement().text('Type your first name and last name, separating the two with a new line character.').hidden('Hint: <code>\\n</code> is the new line character.', 'Hint'))
				.addElement(new ConsoleExpectElement().item('string_console').expect(true, /.+\\n.+/))
				.onFinish()
				.addElement(new TextElement().text('Type a string with three tab characters in a row, followed by at least one letter.').hidden('Hint: <code>\\t</code> is the tab character.', 'Hint'))
				.addElement(new ConsoleExpectElement().item('string_console').expect(true, /\\t\\t\\t[^"]/))
				.onFinish()
				.addElement(new TextElement().text('Type a string that contains only letters and numbers.'))
				.addElement(new ConsoleExpectElement().item('string_console').expect(false, /^[A-z0-9]+$/))
				.onFinish()
				.addElement(new TextElement().text('Type a string that uses concatenation (links two strings together to form a new string).').hidden('Hint: Use <code>+</code> to connect two strings. Each string should have its own starting and ending double quotes, with the + sign between the two strings.', 'Hint'))
				.addElement(new ConsoleExpectElement().item('string_console').expect(true, function(v) { try { var x = eval(v); return v.indexOf('+') !== -1 && x.indexOf('+') === -1; } catch (ex) { return false; } }))
				.onFinish()
		);

		this.addPage(
			new LessonPage('Strings in Programs')
				.addColumns('half half')
				
				.addElement(new JavaEditorElement().column(1).name('string_programs').javaCode('class StringsInPrograms {\n    public static void main(String[] args) {\n    		\n	}\n}'))
				.addElement(new TextElement().text('In a full Java program, typing a string on its own as you did in the Console will not produce any results.'))
				.addElement(new TextElement().text('The syntax structure you learned in the Program Structure lesson apply in a full Java program: class, main method, using semicolons after each instruction.'))
				.onContinue('Start String Program Tutorial')

				.addElement(new EditorPlaybackElement().editor('string_programs').file('strings_printing.txt'))
				.onFinish()
				.onContinue('Start String Challenge')
				
				.addElement(new JavaEditorElement().column(1).name('string_challenge').javaSettings({ className: 'StringsChallenge', includeMain: true }))
				.addElement(new TextElement().text('As a challenge, write a program using the <code>System.out.println</code> and <code>System.out.print</code> commands. Your goal is to draw an image using letters, symbols, and numbers. Create your own methods to represent parts of the drawing.'))

		);

	}
});
