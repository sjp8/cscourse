
$.fn.exists = function() {
	return $(this).length > 0;
};

$.fn.display = $.fn.fadeInCascade = function() {
	var element = $(this);
	var html = $(element).html();
	var text = $(element).text();
	element.html('');
	var $wrap = $('<div />').appendTo(element).css('position', 'relative');
	var $old = $('<div />').appendTo($wrap).html(html);
	var $effect = $('<div />').appendTo($wrap).css('position', 'absolute').css('left', 0).css('top', 0);

	$old.css('opacity', 0.2);

	var chunks = text.split('');

	_(chunks).each(function(chunk, index) {
		_.delay(function() {
			$effect.append($('<span />').text(chunk).fadeIn());
		}, index * 30);
	});

	_.delay(function() {
		$(element).html(html);
	}, chunks.length * 30 + 1);

	return element;
};

$.fn.moveTo = function(replace) {
	var element = $(this);
	var helper = element.clone();

	var previousPositionCss = element.css('position');

	var duration = 2000;

	var position = element.position() || {};
	var size = { width: element.width(), height: element.height() };
	var nextPosition = replace.position() || {};

	element.css('visibility', 'hidden');
	replace.css('visibility', 'hidden');
	helper.appendTo($('body')).css('position', 'absolute').css(position);
	helper.animate(nextPosition, {
		duration: duration,
		complete: function() {
			helper.hide().remove();
			replace.replaceWith(element);
			element.css('visibility', '');
			element.css('position', previousPositionCss);
		}
	});
	replace.animate(size, { duration: duration });
};

$.fn.replaceWithFade = function(replace, onComplete) {
	var element = $(this);
	replace = $(replace);
	
	element.fadeOut(500, function() {
		replace.hide();
		element.replaceWith(replace);
		replace.fadeIn(500, function() { onComplete && onComplete(); });
	});
	return element;
};

$.fn.toggleSlide = function(value) {
	if (value === true && !this.is(':visible')) {
		this.slideDown();
	} else if (value === false && this.is(':visible')) {
		this.slideUp();
	}
	
	return this;
};

