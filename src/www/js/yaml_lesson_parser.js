
var YAMLLessonParser = Class.extend({
	validElements: {
		text: 'TextElement',
		image: 'ImageElement',
		code: 'CodeElement',
		editor: 'EditorElement',
		list: 'ListElement',
		table: 'TableElement',
		quiz: 'QuizElement',
		homework: 'HomeworkElement',
		test: 'HomeworkElement'
	},

	parse: function(file, lesson) {
		var $parser = this;
		
		var _call = function(obj, field, val) {
			if (_.isArray(val)) {
				obj[field].apply(obj, val);
			} else {
				obj[field].call(obj, val);
			}
		};

		$.ajax({
			url: file, type: 'GET', dataType: 'text', async: false,
			success: function(content) {
				console.log('parsing ' + content.substring(0, 100).replace(/\n/g, "\\n") + '...');
				var pages = jsyaml.load(content);
				window.pages = pages;
				
				// content is an array of pages
				_.each(pages, function(page) {
					var lessonPage = new LessonPage(page.title, page.type);
					
					// add columns, copying fields (see js/lesson_column.js methods)
					_.each(page.columns, function(column) {
						var lessonColumn = new LessonColumn();
						_.each(column, function(value, field) {
							if (_.isFunction(lessonColumn[field])) {
								_call(lessonColumn, field, value);
							} else {
								console.error('Cannot apply lesson column field.', [field, value]);
							}
						});
						lessonPage.addColumn(lessonColumn);
					});
					
					// add elements, detecting YAML lesson features, then copying fields
					_.each(page.elements, function(element) {
						var type = _.first(_.keys(element));
						var definition = element[type];

						if (type == 'continue') {
							lessonPage.onContinue(definition);
							return;
						}
						
						var typeClass = $parser.validElements[type];
						if (!typeClass) {
							console.error('Invalid type: ' + type + '. Could not find matching LessonElement implementation.');
							return;
						}

						if (!window[typeClass]) {
							console.error(typeClass + ' is not implemented.');
							return;
						}
						var lessonElement = new window[typeClass]();
						
						if (_.isArray(definition)) {
							if (type == 'text') { lessonElement.text(definition); }
							else if (type == 'list') { lessonElement.items(definition); }
							else { console.error('Unknown array definition application.', [type, definition]); }
						} else if (_.isObject(definition)) {
							_.each(definition, function(value, field) {
								if (_.isFunction(lessonElement[field])) {
									_call(lessonElement, field, value);
								} else {
									console.error('Cannot apply lesson element field.', [field, value]);
								}
							});
						} else if (_.isString(definition)) {
							lessonElement.text(definition);
						} else {
							console.error('Unknown definition application.', [type, definition]);
						}

						lessonPage.addElement(lessonElement);
					});

					lesson.addPage(lessonPage);
				});
			}
		});

	}
	


});