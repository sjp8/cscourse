
var AceAddons = {
	end: function() { this.navigateFileEnd(); },
	endLine: function() { this.navigateLineEnd(); },
	start: function() { this.navigateFileStart(); },
	startLine: function() { this.navigateLineStart(); },
	highlight: function(texts) {
		this.start();
		texts = [].concat(texts);
		this.findAll(_.first(texts));
		_.each(_.rest(texts), function(text) {
			this.findAll(text, null, true);
		}, this);
	},
	
	readOnly: function() {
		this.setReadOnly(true);
		this.setHighlightActiveLine(false);
		this.setHighlightGutterLine(false);
	},

	resizeToFit: function(maxHeight) {
		var editor = this;
		var resize = function() {
			var height = editor.renderer.lineHeight * editor.session.getScreenLength();
			if (!_.isNumber(maxHeight) || height <= maxHeight) {
				editor.resize(true);
				$(editor.renderer.scrollBar.element).hide();
			} else {
				height = maxHeight;
			}
			$(editor.renderer.container).css('height', height);
		};

		editor.renderer.$textLayer.on('changeCharacterSize', function() { resize(); });
	},

	showMessage: function(message, delay, onFinish) {
		var container = $(this.container);
		if (container.find('.editor-message-box').length > 0) {
			container.find('.editor-message-box').remove();
		}
		if (delay && delay < 100) { delay *= 1000; } // change seconds to ms if the number is low.
		var $message = $('<div />').addClass('editor-message-box').html(message.replace(/\n/g, '<br />')).hide();
		$message.appendTo(container);
		$message.fadeIn(300, function() {
			var close = function() { $message.fadeOut(); };
			var finish = function() { onFinish && onFinish(); };
			if (delay) {
				finish();
				_.delay(close, delay);
			} else {
				var $done = $('<div />').text('Close and Continue').addClass('editor-message-box-done').appendTo($message);
				$done.click(function() { close(); finish(); });
			}
		});
	},

	setTimedPlaybackSpeed: function(speedModifier) {
		if (!this.playback) { return; }
		this.playback.speedModifier = speedModifier;
	},

	timedPlayback: function(deltas, messages, onFinished) {
		var i = 0; // delta index
		var m = 0; // message index

		this.playback = {};
		var elapsedTime = 0;
		this.playback.speedModifier = 1.0;
		this.playback.on = true;
		var editor = this;
		messages = messages || [];
		this.playback.func = function(complete) {
			if (complete) {
				// finish remaining change deltas
				editor.getSession().doc.applyDeltas(
					_(deltas).chain()
						.rest(i)
						.filter(function(d) { return (d.type && d.type == "key") || (!d.type && d.action); }).map(function(d) { return d.data; }).value()
				);
				i = deltas.length;
				m = messages.length;
			}

			if (editor.playback.pause) {
				return;
			}

			if (i >= deltas.length && m >= messages.length) {
				clearTimeout(interval);
				editor.playback = {};
				onFinished && onFinished();
				return;
			}

			while (i < deltas.length && deltas[i].time <= elapsedTime) {
				var delta = deltas[i];

				// apply delta
				if (delta.type == 'key' || delta.data.action) {
					editor.getSession().doc.applyDeltas([delta.data]);
				}

				// apply scroll
				else if (delta.type == 'scroll') {
					if (delta.data.top && editor.getSession().getScrollTop() != delta.data.top) editor.renderer.scrollToY(delta.data.top);
					if (delta.data.left && editor.getSession().getScrollLeft() != delta.data.left && $(editor.renderer.scroller).css('overflow-x') == 'scroll') {
						editor.renderer.scrollToX(delta.data.left);
					}
				}

				// apply selection change
				else if (delta.type == 'select') {
					editor.getSession().getSelection().setSelectionRange(delta.data);
				}

				i++;
			}

			// play only one message per loop
			if (m < messages.length && messages[m].time <= elapsedTime) {
				(function() {
					editor.playback.pause = true;
					editor.showMessage(messages[m].message, messages[m].delay || 0, function() {
						editor.playback.pause = false;
					});
				})();
				m++;
			}

			elapsedTime += editor.playback.pause ? 0 : 20 * editor.playback.speedModifier; // simulate speedup by saying that more or less time passed than 20ms.
		};
		var interval = this.playback.interval = setInterval(this.playback.func, 20);
	},

	// completes playback, applying all deltas.
	completeTimedPlayback: function() {
		if (!this.playback.on) { return; }
		this.playback.func(true);
	},

	// interrupts playback, does not fire onFinish
	stopTimedPlayback: function() {
		if (!this.playback.on) { return; }
		clearInterval(this.playback.interval);
		this.playback = {};
	},

	startRecording: function() {
		var editor = this;
		this.record = {};

		if (this.record.recording == true) { return; }
		this.record.recording = true;
		this.record.stack = [];
		this.record.messages = [];

		this.record.startTime = new Date().getTime();
		var timePoint = this.record.timePoint = function() { return new Date().getTime() - editor.record.startTime; };

		this.recordingCallback && this.getSession().removeListener('change', this.recordingCallback);
		this.recordingCallback = function(event) {
			if (editor.record.paused) return;
			editor.record.stack.push({ time: timePoint(), type: "key", data: event.data });
		};

		this.getSession().on('change', this.recordingCallback);

		if (this.recordingCallbackScroll) {
			this.getSession().removeListener('changeScrollTop', this.recordingCallbackScroll);
			this.getSession().removeListener('changeScrollLeft', this.recordingCallbackScroll);
		}
		this.recordingCallbackScroll = function(event) {
			if (editor.record.paused) return;
			var data = {};
			if (event.type == 'changeScrollLeft') {
				data.left = editor.session.getScrollLeft();
				var s = $(editor.renderer.scroller);
				data.cw = s.find('.ace_content').width();
				data.sw = s.width();
			} else if (event.type == 'changeScrollTop') {
				data.top = editor.getSession().getScrollTop();
			}
			editor.record.stack.push({ time: timePoint(), data: data, type: "scroll" });
		};

		this.getSession().on('changeScrollTop', this.recordingCallbackScroll);
		this.getSession().on('changeScrollLeft', this.recordingCallbackScroll);

		this.recordingCallbackSelect && this.getSession().getSelection().removeListener('change', this.recordingCallbackSelect);
		this.recordingCallbackSelection = function(event) {
			if (editor.record.paused) { return; }
			var data = editor.getSession().getSelection().getRange();
			editor.record.stack.push({ time: timePoint(), type: "select", data: data });
		}

		this.getSession().getSelection().on('changeCursor', this.recordingCallbackSelection);
	},

	/**
	 * Adds a recording message with an optional delay.
	 * If no delay is given, 
	 */
	addRecordingMessage: function(message, delay) {
		var m = { message: message, time: this.record.pauseTime - this.record.startTime };
		if (delay) { m.delay = delay; }
		else { m.done = true; }
		this.record.messages.push(m);
		console.log('addRecordingMessage :: ' + m.time);
	},

	resumeRecording: function() {
		if (!this.record.paused) { return; }
		this.record.paused = false;

		// resume
		var diff = new Date().getTime() - this.record.pauseTime;
		this.record.startTime += diff;
		this.record.pauseTime = null;
		this.record.paused = false;
		console.log('resumeRecording :: ' + this.record.timePoint());
	},
	pauseRecording: function() {
		if (!this.record.recording) { return; }
		this.record.paused = true;
		this.record.pauseTime = new Date().getTime();
		console.log('pauseRecording :: ' + this.record.timePoint());
	},

	stopRecording: function() {
		this.record.recording = false;
	},

	getRecordingMessages: function() {
		return this.record.messages;
	},
	getRecordingDeltas: function() {
		return this.record.stack;
	},
	sendRecording: function() {
		var editor = this;
		var filename = prompt('Recording filename:');
		if (filename == null) { return; }
		$.ajax({
			url: 'record.php', type: 'POST',
			data: { content: JSON.stringify({ messages: editor.record.messages, deltas: editor.record.stack }), filename: filename },
			success: function(response) { alert('Saved file: ' + response); }
		});

	}
};
