
var KeypressHandler = Class.extend({
	
	init: function(context) {
		this.onceKeys = {};
		this.alwaysKeys = {};
		this.context = context;
		$('body').keypress(_.bind(function(e) {
			this.handleEvent(e.which);
		}, this));
	},

	handleEvent: function(key) {
		var steps = [];
		var one = this.onceKeys[key];
		if (one) { steps = steps.concat(one); this.onceKeys[key] = []; }

		var al = this.alwaysKeys[key];
		if (al) { steps = steps.concat(al); }
		
		var context = this.context;
		_.each(steps, function(step) { _.bind(step, context)(); });
	},

	once: function(key, func) {
		if (_.isString(key)) { key = key.charAt(0); }
		if (!this.onceKeys[key]) { this.onceKeys[key] = []; }
		this.onceKeys[key].push(func);
	},

	always: function(key, func) {
		if (_.isString(key)) { key = key.charAt(0); }
		if (!this.alwaysKeys[key]) { this.alwaysKeys[key] = []; }
		this.alwaysKeys[key].push(func);
	},

	remove: function(key, func) {
		var one = this.onceKeys[key];
		var al = this.alwaysKeys[key];

		if (one) { one = _.without(one, func); }
		if (al) { al = _.without(al, func); }
	}
});
