
// create a homework pane with coding editor to the left, output/diff to the right, and compilation notices below
// the class is given a list of problems (selecting from dropdown or using < > to move problems), or one problem.
// it can be used as an element (if extended properly) or as the sole feature on a page.

// Other:
// - A Java compiler for the code
// - A diff engine for comparing code output to expected output.
// - A code editor
// - (optional) A navigation class with event connections

var Homework = Class.extend({

	init: function(problem, container) {
		this.problem = problem;
		this.container = container;
		window.homework = this;
		
		this.container.addClass('homework-container');
	},

	show: function() {
		var $editor = this.$editor = $('<div />').addClass('homework-editor');
		$editor.appendTo(this.container);
		
		var $diff = this.$diff = $('<div />').addClass('homework-diff').appendTo(this.container);
		var $compile = this.$compile = $('<div />').addClass('homework-compile').appendTo(this.container);

		$editor.css('height', 200);
		var editor = this.editor = ace.edit($editor[0]);
		_.extend(editor, AceAddons);
		editor.setTheme('ace/theme/textmate');
		editor.getSession().setMode('ace/mode/java');
		editor.setValue('example text', -1);
	}
	
	
	


});
