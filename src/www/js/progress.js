/*
 * progress.js
 * - Enables features for the Progress page
 */

$(function() {
	
	var $sections = $('.sections *[course]').each(function(i, $course) {
		$course = $($course);
		var course = $course.attr('course');
		if (!window[course]) { return; }

		course = new window[course];
		course.setup();

		var pages = _.map(course.pages, function(p) { return '<span class="type-' + p.type + '">' + p.title + '</span>'; }).join(' - ');
		$course.append($('<div />').addClass('course-pages').html(pages));
	});
	
});
