
/**
 * Class for managing the display (of elements and columns) of the page,
 * and data (from Javascript or YAML) for a lesson page. Contains a list of columns and elements.
 */
var LessonPage = Class.extend({

	/**
	 * Constructor.
	 *   Initialize the page with a title and type.
	 *   Valid types: general, practice, java
	 */
	init: function(title, type) {
		this.title = title;
		this.type = type || '';

		this.elements = []; // (permanent) list of elements
		this._columns = []; // (permanent) list of columns
		this.element = $('<div />').addClass('lesson-page'); // container element for page
		this.queue = new Queue(); // a queue used to display elements (resets before each "start" call).

		return this;
	},

	/**
	 * Reset the page in preparation for viewing.
	 */
	reset: function() {
		this.element.empty().remove(); // unbind, delete contents, and remove the element from DOM
		this.queue = new Queue();
	},

	/**
	 * Add an element to the page. Elements should be added in order.
	 */
	addElement: function(element) {
		if (!element) { throw new Error('LessonPage.addElement given a null element.'); }
		this.elements.push(element);
		return this;
	},

	/**
	 * Add Continue element
	 * TODO: deprecate in favor of YAML
	 */
	onContinue: function(buttonTitle) {
		this.addElement(new ContinueButtonElement().text(buttonTitle || 'Continue'));
		return this;
	},

	/**
	 * Add Finish element
	 * TODO: deprecate in favor of YAML
	 */
	onFinish: function() {
		this.addElement(new FinishElement());
		return this;
	},

	/**
	 * Start the page display flow. Prepares elements, then displays the first set of elements.
	 */
	start: function() {
		var self = this;

		// TODO: determine by CSS to modularize the "page"
		this.element.css('height', $(window).height() * .85);

		this.title && $('<div />').addClass('page-title').text(this.title).appendTo(this.element);

		// TODO: two-column layout within a column
		//  |----------| |-----|
		//  ||---||---|| |-----|
		_.each(this.columns(), function(column, index) {
			// insert column and set width/height
			column.reset();
			column.element.css({
				'height': this.element.height(),
				'width': (column.width() - 2) + '%'
			}).attr('column-index', index).appendTo(this.element);
		}, this);

		_(this.elements).each(function(element, index) {
			// FinishElement and ContinueButtonElement are usually blank, and need to be
			// given special behavior.
			if (element instanceof FinishElement) {
				// when the previous element calls onFinish, update both column positions,
				// then continue to the next set of elements
				// TODO: columns should manage themselves
				this.queue.last() && this.queue.last().element.onFinish(function() {
					var startNext = _.after(self.columns().length, function() {
						self.next();
					});
					_.each(self.columns(), function(col) {
						col.setPosition(function() {
							startNext();
						});
					});
				});
				return; // do not add to queue, only setup was necessary.
			} else if (element instanceof ContinueButtonElement) {
				// clicking continue progresses to the next set of elements
				element.once(function() { self.next(); });
			}

			var insert = function(element) {
				if (element.element) {
					var column = self.columns()[element.column()] || self.columns()[0];

					element.reset();

					column.addElement(element); // calls insert code
				} else {
					element.reset();
					element.insert();
				}
			};

			this.queue.add({ element: element, insert: function() { insert(element); } });
		}, this);

		this.next();
	},

	/**
	 * Called by LessonPage when the page's queue is empty.
	 */
	onPageFinish: function(finish) { if (finish) { this._onPageFinish = finish; return this; } else { return this._onPageFinish; } },

	/**
	 * Display the next set of elements in the queue.
	 */
	next: function() {
		var self = this;

		if (this.queue.hasNext()) {

			// display element then move to next item in queue
			var next = this.queue.next();
			next.insert();

			// update column positions after last element in display set is displayed (last designated by
			// either a halting element, or the queue being empty)
			if (!this.queue.hasNext() || next.element.halting()) {
				console.log('LessonPage - waiting for halting element to progress the queue.');
				_.each(this.columns(), function(col) {
					col.setPosition();
				});
			} else {
				// continue adding elements until a halting element (finish or continue)
				this.next();
			}
		}

		if (!this.queue.hasNext()) {
			// queue is empty, let the lesson and other listeners know
			this._onPageFinish && this._onPageFinish();
		}
	},

	/**
	 * Returns true if there are undisplayed elements in the queue.
	 */
	hasNext: function(queue) {
		return this.queue.hasNext(queue);
	},

	/**
	 * Shortcuts for adding columns
	 * TODO: deprecate
	 */
	addColumns: function(type) {
		if (type == 'full width') {
			this.addColumn(new LessonColumn().width(100).highlight());
		} else if (type == 'main terms') {
			this.addColumn(new LessonColumn().width(70).highlight());
			this.addColumn(new LessonColumn().width(30).reverse()).addElement(new TitleElement().column(1).text('List of Terms'));
		} else if (type == 'half half') {
			this.addColumn(new LessonColumn().width(50));
			this.addColumn(new LessonColumn().width(50));
		} else {
			console.error('column preset not recognized.');
		}
		return this;
	},

	/**
	 * Add a LessonColumn to the list of columns for the page.
	 */
	addColumn: function(column) {
		this._columns.push(column);
		return this;
	},

	/**
	 * Returns the list of columns for the page.
	 */
	columns: function() {
		return this._columns;
	},

	/**
	 * Retrieve element by name (set by LessonElement.name method)
	 */
	element: function(name) {
		return _.find(this.elements, function(e) { return e.name && e.name() == name; });
	}

});
