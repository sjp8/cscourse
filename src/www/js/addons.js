
var Queue = Class.extend({
	init: function() { this.queue = []; this.zero = 0;},
	add: function(item) { this.queue.push(item); },
	next: function() { if (this.hasNext()) { return this.queue[this.zero++]; } else { throw new Error('Stack bounds overrun.'); } },
	hasNext: function() { return this.queue.length > this.zero; },
	contains: function(findFunction) { return _.find(this.queue, findFunction); },
	last: function() { return this.queue[this.queue.length - 1]; },
	first: function() { return this.queue[this.zero]; },
	before: function() { return this.queue[this.zero - 1]; },
	findBefore: function(lambda) { return _.chain(this.queue).first(this.zero).reverse().find(lambda).value(); },
	after: function() { return this.queue[this.zero + 1]; },
	undo: function(times) { times = times || 1; this.zero -= times; }

});

var MappedQueue = Class.extend({
	init: function() { this.queue = new Queue(); },
	add: function(queue, item) {
		if (!this.queue[queue]) { this.queue[queue] = []; }
		this.queue[queue].add(item);
	},
	next: function(queue) {
		if (this.hasNext(queue)) {
			return this.queue[queue].next();
		} else throw new Error('Unexpected queue state, called next without elements in queue.');
	},
	hasNext: function(queue) {
		return this.queue[queue] && this.queue[queue].hasNext();
	},
	contains: function(queue, findFunction) {
		return this.queue[queue] && this.queue[queue].contains(findFunction);
	}
});

var Compiler = Class.extend({
	init: function() {
		this.script = 'bsh.php';
	},

	_parseError: function(error) {
		if (error.match(/Sourced file/)) {
			error = error.replace(/Sourced file: (.*)\.tmp :/, '');
		}
		return error;
	},

	runState: function(oldCode, newCode, onFinish) {
		var self = this;
		$.ajax({
			url: this.script, dataType: 'json', type: 'POST',
			data: { code0: oldCode, code1: newCode },
			success: function(response) {
				var outputs = response.outputs;
				onFinish(outputs[0], outputs[1], self._parseError(response.error));
			}
		});
	},

	run: function(code, onFinish) {
		var self = this;
		$.ajax({
			url: this.script, dataType: 'json', type: 'POST',
			data: { code0: code },
			success: function(response) { onFinish(_.first(response.outputs), self._parseError(response.error)); }
		});
	}

});
