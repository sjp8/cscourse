
var Lesson = Class.extend({

	init: function() {
		this.pages = [];
		this.element = $('#lesson');
	},

	setup: function() {
		if (this.file) {
			if (this.file.indexOf('.yml') === this.file.length - 4) {
				new YAMLLessonParser().parse(this.file, this);
			}
		}

	},

	addPage: function(page) {
		this.pages.push(page);
		return this;
	},

	addPageNavigation: function() {
		this.element.find('.page-navigation').remove();
		var $navigation = $('<div />').addClass('page-navigation').appendTo($('#lesson'));
		//var start = $('<div />').addClass('page-button start-page-button').appendTo($navigation);
		var prev = $('<div />').addClass('page-button previous-page-button').appendTo($navigation);
		var current = $('<div />').addClass('page-button current-page-button').appendTo($navigation);
		var next = $('<div />').addClass('page-button next-page-button').appendTo($navigation);

		var navigation = this.navigation = {
			container: $navigation,
			next: {
				container: next,
				text: $('<div />').addClass('button-text').appendTo(next),
				button: $('<div />').addClass('button-click').appendTo(next),
				highlight: function() {
					navigation.next.container.addClass('highlight-suggestion');
				}
			},
			prev: {
				container: prev,
				text: $('<div />').addClass('button-text').appendTo(prev),
				button: $('<div />').addClass('button-click').appendTo(prev)
			},
			current: {
				container: current,
				text: $('<div />').addClass('button-text').appendTo(current),
				button: $('<div />').addClass('button-click').appendTo(current)
			}/*,
			start: {
				container: start,
				text: $('<div />').addClass('button-text').appendTo(start),
				button: $('<div />').addClass('button-click').appendTo(start)
			}*/
		};
	},

	updatePageNavigation: function() {
		if (!this.navigation) { throw new Exception('No page navigation to update.'); return; }
		var self = this, n = this.navigation, i = this.currentPageIndex;
		
		if (!this.navigationColors) {
			var colors = []; // generate a list of 20 colors along the hue spectrum
			_.each(_.range(0, 20), function(index) {
				colors.push(less.tree.functions.spin(new less.tree.Color([255, 0, 0], 0.05), new less.tree.Value(index * (255 / 20))).toCSS());
			});
			colors = _.shuffle(colors);
			this.navigationColors = colors;
		}

		var setupButton = function(button, index, text, toggle) {
			button.text.text(self.pages[index] ? self.pages[index].title : '');
			button.button.text(text);
			button.container.off().click(function() { self.setCurrentPage(index); });
			if (!toggle) { button.container.off(); }
			button.container.toggleClass('page-button-enabled', toggle);
			button.container.toggleClass('page-button-disabled', !toggle);
			button.container.removeClass('highlight-suggestion');
			button.container.css('background-color', self.navigationColors[index % self.navigationColors.length]);
		};

		// setupButton(n.start, 0, 'Restart Lesson', true);
		// n.start.container.css('background-color', '');
		setupButton(n.current, i, 'Restart Page', true);
		setupButton(n.prev, i - 1, 'Previous Page', i > 0);
		setupButton(n.next, i + 1, 'Next Page', i < this.pages.length - 1);
	},

	getCurrentPage: function(page) {
		return this.page;
	},

	setCurrentPage: function(index) {
		this.element.find('.current-lesson-page').remove();
		var pageContainer = $('<div />').addClass('current-lesson-page').appendTo(this.element);

		this.currentPage = this.pages[index];
		this.currentPageIndex = index;

		this.updatePageNavigation();

		this.currentPage.reset();
		this.currentPage.element.appendTo(pageContainer);
		this.currentPage.onPageFinish(_.bind(function() {
			this.navigation.next.highlight();
		}, this));
		this.currentPage.start();
	},

	start: function(startingPage) {
		$('.lesson-container').fadeIn();
		this.setup();
		this.addPageNavigation();
		this.setCurrentPage(startingPage || 0);
	}

});
