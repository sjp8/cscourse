<?php

/*
 * Evaluate java code using BeanShell's bsh.jar
 *
 * Given a file relative to /www/.
 *  or Java code as a string. Temporary files saved in ./bsh_temp/
 */

function getFile($code) {
	$file = tempnam('./bsh_temp/', 'bsh');
	$tempfile = $file;
	file_put_contents($file, $code);

	$file = escapeshellarg($file);
	return $file;
}

$i = 0;
$code = array();

while (array_key_exists('code' . $i, $_REQUEST)) {
	$code[] = $_REQUEST['code' . $i];
	$i++;
}

$files = array();
foreach ($code as $c) {
	$files[] = getFile($c);
}

$command_files = implode(' ', $files);

$command = '"C:\Program Files (x86)\Java\jre7\bin\java.exe" -classpath "../lib/java/;../lib/java/bsh-2.0b4.jar" BeanShellContext ' . $command_files . ' 2>&1';


$outputText = shell_exec($command);
$output = explode("----++++====****----", $outputText); // delimiter in .class file
$outputSize = count($files);

$result = array(
	'outputs' => $output,
	'text' => $outputText,
	'error' => $output[count($output) - 1],
	'command' => $command,
);

echo json_encode($result);

foreach ($files as $file) {
	@unlink($file);
}


?>