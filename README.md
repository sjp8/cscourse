# Computer Science Course (cscourse)

## High-Level Description
CSCourse is comprised of two main areas of functionality:

* Interactive Display Framework: An extensible framework for creating lessons leveraging a variety of interactive elements: text, images, and many other widgets organized within columns, within pages, within a lesson, and finally within a curriculum.
* Computer Science Lessons: index.html, and the lessons folder contain lessons specific to introductory Computer Science, leveraging the framework above.

### Interactive Display Framework

See:

* Lesson.js (all lessons inherit from Lesson, defining a list of LessonPage objects.)
* LessonPage.js (LessonPage is a container for a series of LessonElement objects, which comprise the content.)
* LessonElement.js (This file contains a comprehensive list of element types including text and images. Each element inherits from LessonElement.)
* LessonColumn.js (Added to a LessonPage, any number of columns whose percentage widths total 100%.)

Here is an example of the YAML lesson format:
```yaml
- title: First Page   # the first page title. There can be more than one page per lesson
  type: general                         # the type of lesson, e.g. java, general, homework
  columns:
    - width: 100  # one column at 100% width
  elements:                             # elements that make up the page content
    - text: Hello World!
    - list:
        - First list item.
        - Second list item.
    - continue: # stops showing further elements until the user clicks a continue button
    - text: Further information after clicking continue.
    - code: |   # the code lesson element shows content with monospace font in a selectable code editor.
        System.out.println("Hello World! Java formatted code.");
	#...
- title: Second Page  # the second page title
  type: java
  columns:
    - width: 30
      reverse: true
    - width: 70
  elements:

#...

```

### Computer Science Lessons

See:
* src/www/js/lessons for lessons implemented directly in JavaScript
* src/www/lessons for lessons implemented in YAML.

